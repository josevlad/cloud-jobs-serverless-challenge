locals {
  vjc_dynamo_back_configurations = {
    name             = "vjc-dynamo-back-configurations"
    stream_view_type = "NEW_AND_OLD_IMAGES"
    billing_mode     = "PAY_PER_REQUEST"
    hash_key         = "id"
  }
}

resource "aws_dynamodb_table" "vjc_dynamo_back_configurations" {
  name             = local.vjc_dynamo_back_configurations.name
  hash_key         = local.vjc_dynamo_back_configurations.hash_key
  billing_mode     = local.vjc_dynamo_back_configurations.billing_mode
  stream_view_type = local.vjc_dynamo_back_configurations.stream_view_type

  attribute {
    name = local.vjc_dynamo_back_configurations.hash_key
    type = "S"
  }
}

resource "aws_iam_policy" "vjc_dynamo_back_configurations_iam_policy" {
  name   = "${local.vjc_dynamo_back_configurations.name}-iam-policy"
  policy = jsonencode({
    Version : "2012-10-17",
    Statement : [
      {
        Effect : "Allow",
        Action : [
          "dynamodb:Scan",
          "dynamodb:Query",
          "dynamodb:GetItem",
          "dynamodb:PutItem",
          "dynamodb:UpdateItem",
          "dynamodb:DeleteItem",
          "dynamodb:GetRecords",
          "dynamodb:ListStreams",
          "dynamodb:BatchGetItem",
          "dynamodb:BatchWriteItem",
          "dynamodb:DescribeStream",
          "dynamodb:GetShardIterator",
          "dynamodb:ConditionCheckItem",
        ],
        Resource : [
          aws_dynamodb_table.vjc_dynamo_back_configurations.arn,
          "${aws_dynamodb_table.vjc_dynamo_back_configurations.arn}/*",
          "${aws_dynamodb_table.vjc_dynamo_back_configurations.arn}/index/*"
        ]

      }
    ]
  })
}