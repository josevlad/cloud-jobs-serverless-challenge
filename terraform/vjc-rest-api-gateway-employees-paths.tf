#**********************************************************************************
# API GATEWAY PATH (BASE PATH) /cloud-jobs
resource "aws_api_gateway_resource" "vjc_rest_api_gateway_employees_path_base" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  parent_id   = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.root_resource_id
  path_part   = "cloud-jobs"
}

#**********************************************************************************
# API GATEWAY PATH /cloud-jobs/employee
resource "aws_api_gateway_resource" "vjc_rest_api_gateway_employees_path_employee" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  parent_id   = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_base.id
  path_part   = "employee"
}

#**********************************************************************************
# API GATEWAY PATH /cloud-jobs/employee/{id}
resource "aws_api_gateway_resource" "vjc_rest_api_gateway_employees_path_employee_id" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  parent_id   = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee.id
  path_part   = "{id}"
}