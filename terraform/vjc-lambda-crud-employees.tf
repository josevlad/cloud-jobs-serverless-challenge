data "archive_file" "local_zipped_lambda" {
  type        = "zip"
  source_dir  = "${path.module}/../aws-lambda/dist"
  output_path = "${path.module}/../aws-lambda/dist.zip"
}

resource "aws_lambda_function" "vjc_lambda_crud_employees" {
  function_name = "vjc-lambda-crud-employees"
  filename      = "${path.module}/../aws-lambda/dist.zip"
  role          = aws_iam_role.vjc_lambda_crud_employees_iam_role.arn
  handler       = "index.handler"
  runtime       = "nodejs18.x"

  vpc_config {
    subnet_ids         = module.vpc.private_subnets
    security_group_ids = [aws_security_group.vjc_vpc_security_group.id]
  }

  environment {
    variables = {
      VJC_DYNAMO_BACK_CONFIGURATIONS = aws_dynamodb_table.vjc_dynamo_back_configurations.name
    }
  }
}

resource "aws_cloudwatch_log_group" "vjc_lambda_crud_employees_lg" {
  name              = "/aws/aws-lambda/${aws_lambda_function.vjc_lambda_crud_employees.function_name}"
  retention_in_days = 1
}

#******************************************************************************************
# LAMBDA INTEGRATION TO API GATEWAY
#******************************************************************************************
locals {
  vjc_lambda_crud_employees = {
    endpoints = [
      {
        id          = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee.id
        path        = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee.path
        http_method = aws_api_gateway_method.vjc_rest_api_gateway_employees_method_get_cloud_jobs_employee.http_method
      }, {
        id          = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee_id.id
        path        = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee_id.path
        http_method = aws_api_gateway_method.vjc_rest_api_gateway_employees_method_get_cloud_jobs_employee_id.http_method
      }, {
        id          = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee.id
        path        = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee.path
        http_method = aws_api_gateway_method.vjc_rest_api_gateway_employees_method_post_cloud_jobs_employee.http_method
      }, {
        id          = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee_id.id
        path        = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee_id.path
        http_method = aws_api_gateway_method.vjc_rest_api_gateway_employees_method_put_cloud_jobs_employee_id.http_method
      }, {
        id          = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee_id.id
        path        = aws_api_gateway_resource.vjc_rest_api_gateway_employees_path_employee_id.path
        http_method = aws_api_gateway_method.vjc_rest_api_gateway_employees_method_delete_cloud_jobs_employee_id.http_method
      },
      ##
      {
        id          = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.id
        path        = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.path
        http_method = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_get_cloud_jobs_position.http_method
      }, {
        id          = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
        path        = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.path
        http_method = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_get_cloud_jobs_position_id.http_method
      }, {
        id          = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.id
        path        = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.path
        http_method = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_post_cloud_jobs_position.http_method
      }, {
        id          = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
        path        = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.path
        http_method = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_put_cloud_jobs_position_id.http_method
      }, {
        id          = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
        path        = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.path
        http_method = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_delete_cloud_jobs_position_id.http_method
      }
    ]
  }
}
resource "aws_api_gateway_integration" "vjc_lambda_crud_employees_api_gateway_integration" {
  for_each = {for idx, endpoint in local.vjc_lambda_crud_employees.endpoints : idx => endpoint}

  uri                     = aws_lambda_function.vjc_lambda_crud_employees.invoke_arn
  rest_api_id             = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  http_method             = each.value.http_method
  resource_id             = each.value.id
  type                    = "AWS_PROXY"
  integration_http_method = "POST"
}

resource "aws_lambda_permission" "vjc_lambda_crud_employees_permission" {
  for_each = aws_api_gateway_integration.vjc_lambda_crud_employees_api_gateway_integration

  function_name = aws_lambda_function.vjc_lambda_crud_employees.function_name
  principal     = "apigateway.amazonaws.com"
  action        = "lambda:InvokeFunction"
  statement_id  = each.key
  source_arn    = join("", [
    aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.execution_arn, "/*/",
    each.value.http_method,
    local.vjc_lambda_crud_employees.endpoints[each.key].path
  ])
}
