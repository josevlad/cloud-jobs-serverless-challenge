locals {
  vjc_documentdb_storage = {
    engine             = "docdb"
    engine_version     = "5.0.0"
    family             = "docdb5.0"
    instance_class     = "db.r6g.large"
    name               = "vjc-documentdb-storage"
    identifier         = "vjc-documentdb-storage-identifier"
    cluster_identifier = "vjc-documentdb-storage-cluster-identifier"
  }
}

resource "aws_docdb_subnet_group" "vjc_documentdb_storage_subnet_group" {
  subnet_ids = module.vpc.private_subnets
  name       = "${local.vjc_documentdb_storage.name}-sbg"
}

resource "aws_docdb_cluster_instance" "vjc_documentdb_storage_cluster_instance" {
  cluster_identifier = aws_docdb_cluster.vjc_documentdb_storage_cluster.id
  instance_class     = local.vjc_documentdb_storage.instance_class
  identifier         = local.vjc_documentdb_storage.identifier
}

resource "aws_docdb_cluster" "vjc_documentdb_storage_cluster" {
  db_cluster_parameter_group_name = aws_docdb_cluster_parameter_group.vjc_documentdb_storage_service.name
  db_subnet_group_name            = aws_docdb_subnet_group.vjc_documentdb_storage_subnet_group.name
  cluster_identifier              = local.vjc_documentdb_storage.cluster_identifier
  vpc_security_group_ids          = [aws_security_group.vjc_vpc_security_group.id]
  engine_version                  = local.vjc_documentdb_storage.engine_version
  engine                          = local.vjc_documentdb_storage.engine
  master_username                 = var.vjc_documentdb_storage_user
  master_password                 = var.vjc_documentdb_storage_pass
  skip_final_snapshot             = true
}

resource "aws_docdb_cluster_parameter_group" "vjc_documentdb_storage_service" {
  family = local.vjc_documentdb_storage.family
  name   = "${local.vjc_documentdb_storage.name}-pg"

  parameter {
    name  = "tls"
    value = "disabled"
  }
}
