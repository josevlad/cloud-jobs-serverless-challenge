#**********************************************************************************
# API GATEWAY PATH (BASE PATH) /cloud-jobs
resource "aws_api_gateway_resource" "vjc_rest_api_gateway_positions_path_base" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  parent_id   = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.root_resource_id
  path_part   = "cloud-jobs"
}

#**********************************************************************************
# API GATEWAY PATH /cloud-jobs/position
resource "aws_api_gateway_resource" "vjc_rest_api_gateway_positions_path_position" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  parent_id   = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_base.id
  path_part   = "position"
}

#**********************************************************************************
# API GATEWAY PATH /cloud-jobs/position/{id}
resource "aws_api_gateway_resource" "vjc_rest_api_gateway_positions_path_position_id" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  parent_id   = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.id
  path_part   = "{id}"
}