output "vjc_documentdb_storage_cluster_endpoint" {
  value = aws_docdb_cluster.vjc_documentdb_storage_cluster.endpoint
}

output "vjc_documentdb_storage_cluster_username" {
  value = aws_docdb_cluster.vjc_documentdb_storage_cluster.master_username
}