variable "vjc_rest_api_gateway_employees_stage_name" {
  type    = string
  default = "dev"
}

resource "aws_api_gateway_rest_api" "vjc_rest_api_gateway_employees" {
  name = "vjc-rest-api-gateway-employees"
}

resource "aws_api_gateway_deployment" "vjc_rest_api_gateway_employees_deployment" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  stage_name  = var.vjc_rest_api_gateway_employees_stage_name
  variables   = { deployed_at = timestamp() }
  depends_on  = [

    # GET /cloud-jobs/employee
    aws_api_gateway_method.vjc_rest_api_gateway_employees_method_get_cloud_jobs_employee,

    # GET /cloud-jobs/employee/{id}
    aws_api_gateway_method.vjc_rest_api_gateway_employees_method_get_cloud_jobs_employee_id,

    # POST /cloud-jobs/employee
    aws_api_gateway_method.vjc_rest_api_gateway_employees_method_post_cloud_jobs_employee,

    # PUT /cloud-jobs/employee/{id}
    aws_api_gateway_method.vjc_rest_api_gateway_employees_method_put_cloud_jobs_employee_id,

    # DELETE /cloud-jobs/employee/{id}
    aws_api_gateway_method.vjc_rest_api_gateway_employees_method_delete_cloud_jobs_employee_id,

    #
    # GET /cloud-jobs/position
    aws_api_gateway_method.vjc_rest_api_gateway_positions_method_get_cloud_jobs_position,

    # GET /cloud-jobs/position/{id}
    aws_api_gateway_method.vjc_rest_api_gateway_positions_method_get_cloud_jobs_position_id,

    # POST /cloud-jobs/position
    aws_api_gateway_method.vjc_rest_api_gateway_positions_method_post_cloud_jobs_position,

    # PUT /cloud-jobs/position/{id}
    aws_api_gateway_method.vjc_rest_api_gateway_positions_method_put_cloud_jobs_position_id,

    # DELETE /cloud-jobs/position/{id}
    aws_api_gateway_method.vjc_rest_api_gateway_positions_method_delete_cloud_jobs_position_id,


    # METHOD'S /cloud-jobs/employee/{id}
    aws_api_gateway_integration.vjc_lambda_crud_employees_api_gateway_integration,
  ]
  lifecycle { create_before_destroy = true }
}

resource "aws_api_gateway_rest_api_policy" "vjc_rest_api_gateway_employees_policy" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  policy      = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": "*",
            "Action": "execute-api:Invoke",
            "Resource": "${aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.execution_arn}/*"
        }
    ]
}
EOF
}