#********************************************************************************
# API GATEWAY METHOD /cloud-jobs/position GET
resource "aws_api_gateway_method" "vjc_rest_api_gateway_positions_method_get_cloud_jobs_position" {
  rest_api_id      = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id      = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.id
  http_method      = "GET"
  authorization    = "NONE"
  api_key_required = false
}

resource "aws_api_gateway_method_response" "vjc_rest_api_gateway_positions_method_get_200_cloud_jobs_position" {
  rest_api_id         = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id         = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.id
  http_method         = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_get_cloud_jobs_position.http_method
  status_code         = "200"
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = true
    "method.response.header.Access-Control-Allow-Methods" = true
    "method.response.header.Access-Control-Allow-Origin"  = true
  }
}

resource "aws_api_gateway_integration_response" "vjc_rest_api_gateway_positions_integration_response_get_200_cloud_jobs_position" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.id
  http_method = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_get_cloud_jobs_position.http_method
  status_code = aws_api_gateway_method_response.vjc_rest_api_gateway_positions_method_get_200_cloud_jobs_position.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" : "'Content-Type,x-api-key'"
    "method.response.header.Access-Control-Allow-Methods" : "'GET'"
    "method.response.header.Access-Control-Allow-Origin" : "'*'"
  }

  depends_on = [
    aws_api_gateway_method.vjc_rest_api_gateway_positions_method_get_cloud_jobs_position,
    aws_api_gateway_integration.vjc_lambda_crud_employees_api_gateway_integration
  ]
}

#********************************************************************************
# API GATEWAY METHOD /cloud-jobs/position/{id} GET
resource "aws_api_gateway_method" "vjc_rest_api_gateway_positions_method_get_cloud_jobs_position_id" {
  rest_api_id      = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id      = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
  http_method      = "GET"
  authorization    = "NONE"
  api_key_required = false
}

resource "aws_api_gateway_method_response" "vjc_rest_api_gateway_positions_method_get_200_cloud_jobs_position_id" {
  rest_api_id         = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id         = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
  http_method         = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_get_cloud_jobs_position_id.http_method
  status_code         = "200"
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = true
    "method.response.header.Access-Control-Allow-Methods" = true
    "method.response.header.Access-Control-Allow-Origin"  = true
  }
}

resource "aws_api_gateway_integration_response" "vjc_rest_api_gateway_positions_integration_response_get_200_cloud_jobs_position_id" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
  http_method = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_get_cloud_jobs_position_id.http_method
  status_code = aws_api_gateway_method_response.vjc_rest_api_gateway_positions_method_get_200_cloud_jobs_position_id.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" : "'Content-Type,x-api-key'"
    "method.response.header.Access-Control-Allow-Methods" : "'GET'"
    "method.response.header.Access-Control-Allow-Origin" : "'*'"
  }

  depends_on = [
    aws_api_gateway_method.vjc_rest_api_gateway_positions_method_get_cloud_jobs_position_id,
    aws_api_gateway_integration.vjc_lambda_crud_employees_api_gateway_integration
  ]
}

#*****************************************************************************************
# API GATEWAY METHOD /cloud-jobs/position POST
resource "aws_api_gateway_method" "vjc_rest_api_gateway_positions_method_post_cloud_jobs_position" {
  rest_api_id      = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id      = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.id
  http_method      = "POST"
  authorization    = "NONE"
  api_key_required = false
}

resource "aws_api_gateway_method_response" "vjc_rest_api_gateway_positions_method_post_200_cloud_jobs_position" {
  rest_api_id         = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id         = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.id
  http_method         = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_post_cloud_jobs_position.http_method
  status_code         = "200"
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = true
    "method.response.header.Access-Control-Allow-Methods" = true
    "method.response.header.Access-Control-Allow-Origin"  = true
  }
}

resource "aws_api_gateway_integration_response" "vjc_rest_api_gateway_positions_integration_response_post_200_cloud_jobs_position" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position.id
  http_method = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_post_cloud_jobs_position.http_method
  status_code = aws_api_gateway_method_response.vjc_rest_api_gateway_positions_method_post_200_cloud_jobs_position.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" : "'Content-Type,X-Amz-Date,Authorization,X-Api-Key'"
    "method.response.header.Access-Control-Allow-Methods" : "'*'"
    "method.response.header.Access-Control-Allow-Origin" : "'*'"
  }

  depends_on = [
    aws_api_gateway_method.vjc_rest_api_gateway_positions_method_post_cloud_jobs_position,
    aws_api_gateway_integration.vjc_lambda_crud_employees_api_gateway_integration
  ]
}

#********************************************************************************
# API GATEWAY METHOD /cloud-jobs/position/{id} PUT
resource "aws_api_gateway_method" "vjc_rest_api_gateway_positions_method_put_cloud_jobs_position_id" {
  rest_api_id      = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id      = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
  http_method      = "PUT"
  authorization    = "NONE"
  api_key_required = false
}

resource "aws_api_gateway_method_response" "vjc_rest_api_gateway_positions_method_put_200_cloud_jobs_position_id" {
  rest_api_id         = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id         = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
  http_method         = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_put_cloud_jobs_position_id.http_method
  status_code         = "200"
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = true
    "method.response.header.Access-Control-Allow-Methods" = true
    "method.response.header.Access-Control-Allow-Origin"  = true
  }
}

resource "aws_api_gateway_integration_response" "vjc_rest_api_gateway_positions_integration_response_put_200_cloud_jobs_position_id" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
  http_method = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_put_cloud_jobs_position_id.http_method
  status_code = aws_api_gateway_method_response.vjc_rest_api_gateway_positions_method_put_200_cloud_jobs_position_id.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" : "'Content-Type,X-Amz-Date,Authorization,X-Api-Key'"
    "method.response.header.Access-Control-Allow-Methods" : "'*'"
    "method.response.header.Access-Control-Allow-Origin" : "'*'"
  }

  depends_on = [
    aws_api_gateway_method.vjc_rest_api_gateway_positions_method_put_cloud_jobs_position_id,
    aws_api_gateway_integration.vjc_lambda_crud_employees_api_gateway_integration
  ]
}

#********************************************************************************
# API GATEWAY METHOD /cloud-jobs/position/{id} DELETE
resource "aws_api_gateway_method" "vjc_rest_api_gateway_positions_method_delete_cloud_jobs_position_id" {
  rest_api_id      = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id      = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
  http_method      = "DELETE"
  authorization    = "NONE"
  api_key_required = false
}

resource "aws_api_gateway_method_response" "vjc_rest_api_gateway_positions_method_delete_200_cloud_jobs_position_id" {
  rest_api_id         = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id         = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
  http_method         = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_delete_cloud_jobs_position_id.http_method
  status_code         = "200"
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = true
    "method.response.header.Access-Control-Allow-Methods" = true
    "method.response.header.Access-Control-Allow-Origin"  = true
  }
}

resource "aws_api_gateway_integration_response" "vjc_rest_api_gateway_positions_integration_response_delete_200_cloud_jobs_position_id" {
  rest_api_id = aws_api_gateway_rest_api.vjc_rest_api_gateway_employees.id
  resource_id = aws_api_gateway_resource.vjc_rest_api_gateway_positions_path_position_id.id
  http_method = aws_api_gateway_method.vjc_rest_api_gateway_positions_method_delete_cloud_jobs_position_id.http_method
  status_code = aws_api_gateway_method_response.vjc_rest_api_gateway_positions_method_delete_200_cloud_jobs_position_id.status_code

  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" : "'Content-Type,X-Amz-Date,Authorization,X-Api-Key'"
    "method.response.header.Access-Control-Allow-Methods" : "'*'"
    "method.response.header.Access-Control-Allow-Origin" : "'*'"
  }

  depends_on = [
    aws_api_gateway_method.vjc_rest_api_gateway_positions_method_delete_cloud_jobs_position_id,
    aws_api_gateway_integration.vjc_lambda_crud_employees_api_gateway_integration
  ]
}