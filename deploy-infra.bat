
for /F "tokens=*" %%A in ('cd') do set "ROOT_DIR=%%A"

set TERRAFORM_DIR=%ROOT_DIR%\terraform

cd /D "%TERRAFORM_DIR%"

rmdir /s /q .terraform
del .terraform.lock.hcl

terraform init
terraform plan
terraform apply -auto-approve

rmdir /s /q aws-lambda

cd /D "%ROOT_DIR%"
