import { isEmpty, overwriteSystemLogsPinoForNewrelic } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { ApiGatewayErrorResponse, APIGatewayEvent, ApiGatewayProxyResponse, Context, HttpStatus } from './src/types';
import { ApiGatewayEvenResolversRegistry } from './src/adapters';

overwriteSystemLogsPinoForNewrelic();
export const handler = async (event: APIGatewayEvent, context: Context) => {
  console.debug('INDEX.HANDLER.ERROR', { event });
  context.callbackWaitsForEmptyEventLoop = false;
  const proxyResponse = new ApiGatewayProxyResponse();
  try {
    const proxyResolver = ApiGatewayEvenResolversRegistry.getResolver(event);
    const response = await proxyResolver.resolve(event);
    proxyResponse.setResponse(response);
  } catch (err) {
    const { body, path, pathParameters, queryStringParameters, httpMethod, resource } = event;
    console.error('INDEX.HANDLER.ERROR', {
      err,
      message: err.message,
      event: { body, path, pathParameters, queryStringParameters, httpMethod, resource }
    });
    proxyResponse.setResponse(
      new ApiGatewayErrorResponse({
        errorType: isEmpty(err.errorType) ? 'INTERNAL_SERVER_ERROR' : err.errorType,
        errorMessage: err.message,
        statusCode: isEmpty(err.code) ? HttpStatus.INTERNAL_SERVER_ERROR : err.code,
        data: { body, path, pathParameters, queryStringParameters, httpMethod, resource }
      })
    );
  }
  return proxyResponse.getResponse();
};
