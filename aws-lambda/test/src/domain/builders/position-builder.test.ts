import { BusinessException } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { PositionBuilder, SchemaValidatorRegistry } from '../../../../src/domain';
import { POSITION_JSON_ARRAY } from '../../../mocks';
import { TestUtils } from '../../../test-utils';
import { HttpStatus } from '../../../../src/types';

describe('POSITION_BUILDER_SUITE_TEST', () => {
  beforeEach(() => {
    TestUtils.registerSchemasForTest();
  });
  afterEach(() => {
    SchemaValidatorRegistry.registry.clear();
  });

  it('SHOULD_POSITION_BUILDER_MUST_CREATED', async () => {
    // GIVEN
    const [data] = POSITION_JSON_ARRAY;
    // WHEN
    const position = new PositionBuilder().setName(data.name).setDescription(data.description).build();
    //THEN
    expect(position.constructor.name).toEqual('Position');
  });

  it('SHOULD_POSITION_BUILDER_MUST_CREATED_FROM_JSON', async () => {
    // GIVEN
    const [data] = POSITION_JSON_ARRAY;
    // WHEN
    const position = new PositionBuilder().build(data);
    //THEN
    expect(position.constructor.name).toEqual('Position');
  });

  it('SHOULD_POSITION_BUILDER_MUST_THROW_ERROR', () => {
    try {
      // WHEN
      new PositionBuilder().build();
    } catch (err) {
      // THEN
      const { errorType, code, message } = err as BusinessException;
      expect(errorType).toEqual('SHIELD_VALIDATOR_SCHEMA_ERROR');
      expect(code).toEqual(HttpStatus.BAD_REQUEST);
      expect(message).toEqual('name is required');
    }
  });

  it('SHOULD_POSITION_BUILDER_MUST_THROW_ERROR_V2', () => {
    try {
      // WHEN
      new PositionBuilder().build({ name: 'developer', description: '!@#$%^&*()' });
    } catch (err) {
      // THEN
      const { errorType, code, message } = err as BusinessException;
      expect(errorType).toEqual('SHIELD_VALIDATOR_SCHEMA_ERROR');
      expect(code).toEqual(HttpStatus.BAD_REQUEST);
      expect(message).toEqual('description has an invalid pattern');
    }
  });

  it('SHOULD_POSITION_BUILDER_MUST_THROW_ERROR_V3', () => {
    try {
      // WHEN
      new PositionBuilder().build({ name: 'AB', description: TestUtils.generateAlphabeticString(120) });
    } catch (err) {
      // THEN
      const { errorType, code, message } = err as BusinessException;
      expect(errorType).toEqual('SHIELD_VALIDATOR_SCHEMA_ERROR');
      expect(code).toEqual(HttpStatus.BAD_REQUEST);
      expect(message).toEqual('name must have a minimum of 3 characters,description must have a maximum of 100 characters');
    }
  });
});
