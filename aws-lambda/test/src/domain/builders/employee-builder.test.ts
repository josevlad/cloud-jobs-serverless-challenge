import { BusinessException, clone } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { Employee, EmployeeBuilder, Position, SchemaValidatorRegistry } from '../../../../src/domain';
import { EMPLOYEE_JSON_ARRAY } from '../../../mocks/domain/EMPLOYEE_JSON_ARRAY';
import { POSITION_JSON_ARRAY } from '../../../mocks';
import { TestUtils } from '../../../test-utils';
import { HttpStatus } from '../../../../src/types';

describe('EMPLOYEE_BUILDER_SUITE_TEST', () => {
  beforeEach(() => {
    TestUtils.registerSchemasForTest();
  });
  afterEach(() => {
    SchemaValidatorRegistry.registry.clear();
  });

  it('SHOULD_EMPLOYEE_BUILDER_MUST_CREATED', async () => {
    // GIVEN
    const [data] = clone<Employee[]>(EMPLOYEE_JSON_ARRAY);
    const position = clone<Position>(POSITION_JSON_ARRAY[0]);
    position.id = '5fcda9a6fbd0b214d0438952';
    // WHEN
    const employee = new EmployeeBuilder()
      .setName(data.name)
      .setNationalIdentityCard(data.nationalIdentityCard)
      .setAge(data.age)
      .setPosition(position)
      .build();
    //THEN
    expect(employee.constructor.name).toEqual('Employee');
  });

  it('SHOULD_EMPLOYEE_BUILDER_MUST_CREATED_FROM_JSON', async () => {
    // GIVEN
    const [data] = EMPLOYEE_JSON_ARRAY;
    // WHEN
    const employee = new EmployeeBuilder().build(data);
    //THEN
    expect(employee.constructor.name).toEqual('Employee');
  });

  it('SHOULD_EMPLOYEE_MUST_THROW_ERROR', () => {
    try {
      // WHEN
      new EmployeeBuilder().build();
    } catch (err) {
      // THEN
      const { errorType, code, message } = err as BusinessException;
      expect(errorType).toEqual('SHIELD_VALIDATOR_SCHEMA_ERROR');
      expect(code).toEqual(HttpStatus.BAD_REQUEST);
      expect(message).toEqual('name is required,nationalIdentityCard is required,age is required,position is required');
    }
  });

  it('SHOULD_EMPLOYEE_MUST_THROW_ERROR_V2', () => {
    try {
      // WHEN
      new EmployeeBuilder().build({
        name: '!@#$%^&*()',
        nationalIdentityCard: 'is not number',
        age: 'is not number'
      });
    } catch (err) {
      // THEN
      const { errorType, code, message } = err as BusinessException;
      expect(errorType).toEqual('SHIELD_VALIDATOR_SCHEMA_ERROR');
      expect(code).toEqual(HttpStatus.BAD_REQUEST);
      expect(message).toEqual(
        'name has an invalid pattern,nationalIdentityCard must have only numbers,age must have only numbers,position is required'
      );
    }
  });

  it('SHOULD_EMPLOYEE_MUST_THROW_ERROR_V2', () => {
    try {
      // WHEN
      new EmployeeBuilder().build({
        name: 'Vl4dim1r',
        nationalIdentityCard: 12456789,
        age: 17,
        position: POSITION_JSON_ARRAY[0]
      });
    } catch (err) {
      // THEN
      const { errorType, code, message } = err as BusinessException;
      expect(errorType).toEqual('SHIELD_VALIDATOR_SCHEMA_ERROR');
      expect(code).toEqual(HttpStatus.BAD_REQUEST);
      expect(message).toEqual('name has an invalid pattern,age must be at least 18');
    }
  });

  it('SHOULD_EMPLOYEE_MUST_THROW_ERROR_V4', () => {
    try {
      // WHEN
      new EmployeeBuilder().build({
        name: 'AB',
        nationalIdentityCard: 12456789,
        age: 80,
        position: POSITION_JSON_ARRAY[0]
      });
    } catch (err) {
      // THEN
      const { errorType, code, message } = err as BusinessException;
      expect(errorType).toEqual('SHIELD_VALIDATOR_SCHEMA_ERROR');
      expect(code).toEqual(HttpStatus.BAD_REQUEST);
      expect(message).toEqual('name must have a minimum of 3 characters,age must be at most 65');
    }
  });
});
