import * as dotenv from 'dotenv';
import { clone } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { DynamoDBClient, GetItemCommandOutput } from '@aws-sdk/client-dynamodb';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { marshall } from '@aws-sdk/util-dynamodb';
// @ts-ignore
import path from 'path';

import { MONGODB_SETTINGS_MOCK } from '../../../mocks/dynamodb/MONGODB_SETTINGS.mock';
import { IRepository, PositionMongoRepository } from '../../../../src/ports';
import { Position, SchemaValidatorRegistry } from '../../../../src/domain';
import { DataBaseService } from '../../../../src/services';
import { HttpStatus, SearchStringency } from '../../../../src/types';
import { POSITION_JSON_ARRAY } from '../../../mocks';
import { TestUtils } from '../../../test-utils';

dotenv.config({ path: path.resolve(__dirname, '../../../.env.test') });
jest.setTimeout(1000000);

describe('POSITION_MONGO_REPOSITORY_SUITE_TEST', () => {
  const mockSendDDB = jest.fn();
  let mongoMemoryServer: MongoMemoryReplSet;
  let mongoRepository: IRepository<Position>;

  beforeEach(async () => {
    jest.clearAllMocks();
    TestUtils.registerSchemasForTest();
    const { mongoServer, mongoServerHost } = await TestUtils.startMongoServerInMemory();
    MONGODB_SETTINGS_MOCK.host = mongoServerHost;
    mongoMemoryServer = mongoServer;
    DynamoDBClient.prototype.send = mockSendDDB;
    mongoRepository = new PositionMongoRepository();
  });

  afterEach(async () => {
    jest.clearAllMocks();
    jest.resetAllMocks();
    await DataBaseService.dbDisconnect();
    SchemaValidatorRegistry.registry.clear();
    await TestUtils.stopMongoServerInMemory(mongoMemoryServer);
  });

  it('SHOULD_CREATE_A_NEW_POSITION', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const entity = clone<Position>(POSITION_JSON_ARRAY[0]);
    const repositoryActionResult = await mongoRepository.save(entity);
    expect(repositoryActionResult.success).toEqual(true);
    expect(repositoryActionResult.message).toEqual('Position successfully saved');
    expect(repositoryActionResult.entity.id).toMatch(/^[0-9a-fA-F]{24}$/);
  });

  it('SHOULD_FIND_AN_POSITION', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    const entity = clone<Position>(POSITION_JSON_ARRAY[0]);
    const repositoryActionResult = await mongoRepository.findOneByFilters(entity);
    expect(repositoryActionResult.name).toEqual(entity.name);
    expect(repositoryActionResult.description).toEqual(entity.description);
    expect(repositoryActionResult.id).toMatch(/^[0-9a-fA-F]{24}$/);
  });

  it('SHOULD_POSITION_NOT_FOUND', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    const repositoryActionResult = await mongoRepository.findOneByFilters({ name: 'POSITION_NOT_FOUND' });
    expect(repositoryActionResult).toBeNull();
  });

  it('SHOULD_UPDATE_POSITION', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    const inDB = await mongoRepository.findOneByFilters(POSITION_JSON_ARRAY[0]);
    const toUpdate = clone<Position>(inDB);
    toUpdate.name = POSITION_JSON_ARRAY[1].name;
    toUpdate.description = POSITION_JSON_ARRAY[1].description;
    const repositoryActionResult = await mongoRepository.update(clone(toUpdate));
    expect(repositoryActionResult.success).toEqual(true);
    expect(repositoryActionResult.message).toEqual('Position successfully updated');
    expect(repositoryActionResult.entity.name).toEqual(POSITION_JSON_ARRAY[1].name);
    expect(repositoryActionResult.entity.description).toEqual(POSITION_JSON_ARRAY[1].description);
    expect(repositoryActionResult.entity.id).toEqual(inDB.id);
  });

  it('SHOULD_NOT_UPDATE_POSITION', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    try {
      await mongoRepository.update(clone(POSITION_JSON_ARRAY[0]));
    } catch (err) {
      expect(err.errorType).toEqual('INTERNAL_SERVER_ERROR');
      expect(err.message).toEqual('Argument passed in must be a single String of 12 bytes or a string of 24 hex characters');
      expect(err.code).toEqual(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it('SHOULD_FIND_ALL_POSITION', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    const repositoryActionResult = await mongoRepository.findAll();
    expect(repositoryActionResult.page).toEqual(1);
    expect(repositoryActionResult.totalRecords).toEqual(10);
    expect(repositoryActionResult.totalPages).toEqual(1);
    expect(repositoryActionResult.payload.length).toEqual(10);
  });

  it('SHOULD_FIND_ALL_POSITION_PAGINATED', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    let repositoryActionResult;
    repositoryActionResult = await mongoRepository.findAll({ limit: 2 });
    expect(repositoryActionResult.page).toEqual(1);
    expect(repositoryActionResult.totalRecords).toEqual(10);
    expect(repositoryActionResult.totalPages).toEqual(5);
    expect(repositoryActionResult.payload.length).toEqual(2);

    const [verify] = repositoryActionResult.payload;

    repositoryActionResult = await mongoRepository.findAll({ limit: 2, page: 2 });
    expect(repositoryActionResult.page).toEqual(2);
    expect(repositoryActionResult.totalRecords).toEqual(10);
    expect(repositoryActionResult.totalPages).toEqual(5);
    expect(repositoryActionResult.payload.length).toEqual(2);

    expect(repositoryActionResult.payload[0].id).not.toEqual(verify.id);
  });

  it('SHOULD_THROW_PAGE_OUT_OF_RANGE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    try {
      await mongoRepository.findAll({ page: 50 });
    } catch (err) {
      expect(err.message).toEqual(`page 50 is out of range in the query`);
      expect(err.errorType).toEqual('PAGE_OUT_OF_RANGE');
      expect(err.code).toEqual(HttpStatus.BAD_REQUEST);
    }
  });

  it('SHOULD_FIND_ALL_POSITION_BY_FILTERS', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    const repositoryActionResult = await mongoRepository.findAll({
      stringency: SearchStringency.CONTAINS,
      description: 'Encargado'
    });
    expect(repositoryActionResult.page).toEqual(1);
    expect(repositoryActionResult.totalRecords).toEqual(6);
    expect(repositoryActionResult.totalPages).toEqual(1);
    expect(repositoryActionResult.payload.length).toEqual(6);
  });

  it('SHOULD_FIND_ALL_POSITION_BY_FILTERS_V2', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    const repositoryActionResult = await mongoRepository.findAll({
      stringency: SearchStringency.EXACTLY,
      name: 'Analista de Datos'
    });
    expect(repositoryActionResult.page).toEqual(1);
    expect(repositoryActionResult.totalRecords).toEqual(1);
    expect(repositoryActionResult.totalPages).toEqual(1);
    expect(repositoryActionResult.payload.length).toEqual(1);
  });

  it('SHOULD_DELETE_AN_POSITION_BY_ID', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    const toDelete = await mongoRepository.findOneByFilters({ name: POSITION_JSON_ARRAY[0].name });
    const repositoryActionResult = await mongoRepository.delete(toDelete.id);
    const verify = await mongoRepository.findOneByFilters({ id: toDelete.id });
    expect(repositoryActionResult.success).toEqual(true);
    expect(verify).toBeNull();
  });

  it('SHOULD_THROW_ERROR_TO_TRY_DELETE_AN_POSITION', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { positions } = await TestUtils.loadAllDataInDB();
    const toDelete = clone<Position>(positions[0]);
    toDelete.id = '5fcda9a6fbd0b214d0438952';
    try {
      await mongoRepository.delete(toDelete.id);
    } catch (err) {
      expect(err.errorType).toEqual('NOT_FOUND');
      expect(err.message).toEqual(`record not found with id: 5fcda9a6fbd0b214d0438952`);
      expect(err.code).toEqual(HttpStatus.NOT_FOUND);
    }
  });
});
