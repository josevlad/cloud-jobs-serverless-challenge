import * as dotenv from 'dotenv';
import { clone } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { DynamoDBClient, GetItemCommandOutput } from '@aws-sdk/client-dynamodb';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { marshall } from '@aws-sdk/util-dynamodb';
// @ts-ignore
import path from 'path';

import { MONGODB_SETTINGS_MOCK } from '../../../mocks/dynamodb/MONGODB_SETTINGS.mock';
import { EMPLOYEE_JSON_ARRAY } from '../../../mocks/domain/EMPLOYEE_JSON_ARRAY';
import { Employee, SchemaValidatorRegistry } from '../../../../src/domain';
import { EmployeeMongoRepository, IRepository } from '../../../../src/ports';
import { DataBaseService } from '../../../../src/services';
import { HttpStatus, SearchStringency } from '../../../../src/types';
import { TestUtils } from '../../../test-utils';

dotenv.config({ path: path.resolve(__dirname, '../../../.env.test') });
jest.setTimeout(1000000);

describe('EMPLOYEE_MONGO_REPOSITORY_SUITE_TEST', () => {
  const mockSendDDB = jest.fn();
  let mongoMemoryServer: MongoMemoryReplSet;
  let mongoRepository: IRepository<Employee>;

  beforeEach(async () => {
    jest.clearAllMocks();
    TestUtils.registerSchemasForTest();
    const { mongoServer, mongoServerHost } = await TestUtils.startMongoServerInMemory();
    MONGODB_SETTINGS_MOCK.host = mongoServerHost;
    mongoMemoryServer = mongoServer;
    DynamoDBClient.prototype.send = mockSendDDB;
    mongoRepository = new EmployeeMongoRepository();
  });

  afterEach(async () => {
    jest.clearAllMocks();
    jest.resetAllMocks();
    await DataBaseService.dbDisconnect();
    SchemaValidatorRegistry.registry.clear();
    await TestUtils.stopMongoServerInMemory(mongoMemoryServer);
  });

  it('SHOULD_CREATE_A_NEW_EMPLOYEE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const [firstPositionInDB] = await TestUtils.loadAllPositionInDB();
    const entity = clone<Employee>(EMPLOYEE_JSON_ARRAY[0]);
    entity.position = {
      id: firstPositionInDB.id.toString(),
      name: firstPositionInDB.name,
      description: firstPositionInDB.description
    };
    const repositoryActionResult = await mongoRepository.save(entity);
    expect(repositoryActionResult.success).toEqual(true);
    expect(repositoryActionResult.message).toEqual('Employee successfully saved');
    expect(repositoryActionResult.entity.id).toMatch(/^[0-9a-fA-F]{24}$/);
  });

  it('SHOULD_THROW_ERROR_TO_TRY_CREATE_A_NEW_EMPLOYEE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    const entity = clone<Employee>(EMPLOYEE_JSON_ARRAY[0]);
    entity.position = { id: '5fcda9a6fbd0b214d0438952', name: '', description: '' };
    try {
      await mongoRepository.save(entity);
    } catch (err) {
      expect(err.errorType).toEqual('NOT_FOUND');
      expect(err.message).toEqual(`Position ID provided does not exist in db: 5fcda9a6fbd0b214d0438952.`);
      expect(err.code).toEqual(HttpStatus.NOT_FOUND);
    }
  });

  it('SHOULD_FIND_AN_EMPLOYEE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const entity = clone<Employee>(EMPLOYEE_JSON_ARRAY[0]);
    const repositoryActionResult = await mongoRepository.findOneByFilters(entity);
    expect(repositoryActionResult.name).toEqual(entity.name);
    expect(repositoryActionResult.nationalIdentityCard).toEqual(entity.nationalIdentityCard);
    expect(repositoryActionResult.age).toEqual(entity.age);
    expect(repositoryActionResult.position).not.toBeNull();
    expect(repositoryActionResult.id).toMatch(/^[0-9a-fA-F]{24}$/);
  });

  it('SHOULD_EMPLOYEE_NOT_FOUND', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repositoryActionResult = await mongoRepository.findOneByFilters({ name: 'POSITION_NOT_FOUND' });
    expect(repositoryActionResult).toBeNull();
  });

  it('SHOULD_UPDATE_EMPLOYEE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees, positions } = await TestUtils.loadAllDataInDB();
    const inDB = employees[0];
    const toUpdate = clone<Employee>({ ...inDB, ...{ position: { id: '' } } });
    toUpdate.id = inDB.id.toString();
    toUpdate.name = 'Tony Stark';
    toUpdate.age = 53;
    toUpdate.nationalIdentityCard = 369258142;
    toUpdate.position.id = positions[5].id.toString();
    const repositoryActionResult = await mongoRepository.update(clone(toUpdate));
    expect(repositoryActionResult.success).toEqual(true);
    expect(repositoryActionResult.message).toEqual('Employee successfully updated');
    expect(repositoryActionResult.entity.name).toEqual('Tony Stark');
    expect(repositoryActionResult.entity.nationalIdentityCard).toEqual(369258142);
    expect(repositoryActionResult.entity.age).toEqual(53);
    expect(repositoryActionResult.entity.id).toEqual(inDB.id.toString());
    expect(repositoryActionResult.entity.position.id).not.toEqual(inDB.position.id.toString());
  });

  it('SHOULD_UPDATE_EMPLOYEE_V2', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    const inDB = employees[0];
    const toUpdate = clone<Employee>(inDB);
    toUpdate.id = inDB.id.toString();
    toUpdate.name = 'Tony Stark';
    toUpdate.age = 53;
    toUpdate.nationalIdentityCard = 369258142;
    const repositoryActionResult = await mongoRepository.update(clone(toUpdate));
    expect(repositoryActionResult.success).toEqual(true);
    expect(repositoryActionResult.message).toEqual('Employee successfully updated');
    expect(repositoryActionResult.entity.name).toEqual('Tony Stark');
    expect(repositoryActionResult.entity.nationalIdentityCard).toEqual(369258142);
    expect(repositoryActionResult.entity.age).toEqual(53);
    expect(repositoryActionResult.entity.id).toEqual(inDB.id.toString());
    expect(repositoryActionResult.entity.position.id).toEqual(employees[0].position.id.toString());
  });

  it('SHOULD_UPDATE_EMPLOYEE_V3', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    const inDB = employees[0];
    const toUpdate = clone<Employee>(inDB);
    toUpdate.id = inDB.id.toString();
    toUpdate.name = 'Tony Stark';
    toUpdate.age = 53;
    toUpdate.nationalIdentityCard = 369258142;
    delete toUpdate.position;
    const repositoryActionResult = await mongoRepository.update(clone(toUpdate));
    expect(repositoryActionResult.success).toEqual(true);
    expect(repositoryActionResult.message).toEqual('Employee successfully updated');
    expect(repositoryActionResult.entity.name).toEqual('Tony Stark');
    expect(repositoryActionResult.entity.nationalIdentityCard).toEqual(369258142);
    expect(repositoryActionResult.entity.age).toEqual(53);
    expect(repositoryActionResult.entity.id).toEqual(inDB.id.toString());
    expect(repositoryActionResult.entity.position.id).toEqual(employees[0].position.id.toString());
  });

  it('SHOULD_THROW_ERROR_TO_TRY_UPDATE_A_NEW_EMPLOYEE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    const entity = clone<Employee>(employees[0]);
    entity.position = { id: '5fcda9a6fbd0b214d0438952', name: '', description: '' };
    try {
      await mongoRepository.save(entity);
    } catch (err) {
      expect(err.errorType).toEqual('NOT_FOUND');
      expect(err.message).toEqual(`Position ID provided does not exist in db: 5fcda9a6fbd0b214d0438952.`);
      expect(err.code).toEqual(HttpStatus.NOT_FOUND);
    }
  });

  it('SHOULD_FIND_ALL_EMPLOYEEN', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repositoryActionResult = await mongoRepository.findAll();
    expect(repositoryActionResult.page).toEqual(1);
    expect(repositoryActionResult.totalRecords).toEqual(10);
    expect(repositoryActionResult.totalPages).toEqual(1);
    expect(repositoryActionResult.payload.length).toEqual(10);
  });

  it('SHOULD_FIND_ALL_EMPLOYEEN_PAGINATED', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    let repositoryActionResult;
    repositoryActionResult = await mongoRepository.findAll({ limit: 2 });
    expect(repositoryActionResult.page).toEqual(1);
    expect(repositoryActionResult.totalRecords).toEqual(10);
    expect(repositoryActionResult.totalPages).toEqual(5);
    expect(repositoryActionResult.payload.length).toEqual(2);

    const [verify] = repositoryActionResult.payload;

    repositoryActionResult = await mongoRepository.findAll({ limit: 2, page: 2 });
    expect(repositoryActionResult.page).toEqual(2);
    expect(repositoryActionResult.totalRecords).toEqual(10);
    expect(repositoryActionResult.totalPages).toEqual(5);
    expect(repositoryActionResult.payload.length).toEqual(2);

    expect(repositoryActionResult.payload[0].id).not.toEqual(verify.id);
  });

  it('SHOULD_THROW_PAGE_OUT_OF_RANGE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    try {
      await mongoRepository.findAll({ page: 50 });
    } catch (err) {
      expect(err.message).toEqual(`page 50 is out of range in the query`);
      expect(err.errorType).toEqual('PAGE_OUT_OF_RANGE');
      expect(err.code).toEqual(HttpStatus.BAD_REQUEST);
    }
  });

  it('SHOULD_FIND_ALL_EMPLOYEEN_BY_FILTERS', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repositoryActionResult = await mongoRepository.findAll({
      stringency: SearchStringency.CONTAINS,
      name: 'o'
    });
    expect(repositoryActionResult.page).toEqual(1);
    expect(repositoryActionResult.totalRecords).toEqual(5);
    expect(repositoryActionResult.totalPages).toEqual(1);
    expect(repositoryActionResult.payload.length).toEqual(5);
  });

  it('SHOULD_FIND_ALL_EMPLOYEEN_BY_FILTERS_V2', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repositoryActionResult = await mongoRepository.findAll({
      stringency: SearchStringency.EXACTLY,
      name: 'John Doe',
      age: 30
    });
    expect(repositoryActionResult.page).toEqual(1);
    expect(repositoryActionResult.totalRecords).toEqual(1);
    expect(repositoryActionResult.totalPages).toEqual(1);
    expect(repositoryActionResult.payload.length).toEqual(1);
  });

  it('SHOULD_DELETE_AN_EMPLOYEEN_BY_ID', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    const toDelete = clone<Employee>(employees[0]);
    const repositoryActionResult = await mongoRepository.delete(toDelete.id);
    const verify = await mongoRepository.findOneByFilters({ id: toDelete.id });
    expect(repositoryActionResult.success).toEqual(true);
    expect(verify).toBeNull();
  });

  it('SHOULD_THROW_ERROR_TO_TRY_DELETE_AN_EMPLOYEE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    const toDelete = clone<Employee>(employees[0]);
    toDelete.id = '5fcda9a6fbd0b214d0438952';
    try {
      await mongoRepository.delete(toDelete.id);
    } catch (err) {
      expect(err.errorType).toEqual('NOT_FOUND');
      expect(err.message).toEqual(`record not found with id: 5fcda9a6fbd0b214d0438952`);
      expect(err.code).toEqual(HttpStatus.NOT_FOUND);
    }
  });
});
