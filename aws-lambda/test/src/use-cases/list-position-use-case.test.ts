import * as dotenv from 'dotenv';
import { DynamoDBClient, GetItemCommandOutput } from '@aws-sdk/client-dynamodb';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { marshall } from '@aws-sdk/util-dynamodb';
// @ts-ignore
import path from 'path';

import { MONGODB_SETTINGS_MOCK } from '../../mocks/dynamodb/MONGODB_SETTINGS.mock';
import { SchemaValidatorRegistry } from '../../../src/domain';
import { ListPositionUseCase } from '../../../src/use-cases';
import { PositionMongoRepository } from '../../../src/ports';
import { DataBaseService } from '../../../src/services';
import { TestUtils } from '../../test-utils';
import { HttpStatus, SearchStringency } from '../../../src/types';

dotenv.config({ path: path.resolve(__dirname, '../../.env.test') });
jest.setTimeout(1000000);

describe('LIST_POSITION_USE_CASE_SUITE_TEST', () => {
  const mockSendDDB = jest.fn();
  let mongoMemoryServer: MongoMemoryReplSet;

  beforeEach(async () => {
    jest.clearAllMocks();
    TestUtils.registerSchemasForTest();
    const { mongoServer, mongoServerHost } = await TestUtils.startMongoServerInMemory();
    MONGODB_SETTINGS_MOCK.host = mongoServerHost;
    mongoMemoryServer = mongoServer;
    DynamoDBClient.prototype.send = mockSendDDB;
  });

  afterEach(async () => {
    jest.clearAllMocks();
    jest.resetAllMocks();
    await DataBaseService.dbDisconnect();
    SchemaValidatorRegistry.registry.clear();
    await TestUtils.stopMongoServerInMemory(mongoMemoryServer);
  });

  it('SHOULD_FIND_ALL_POSITION_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const useCase = new ListPositionUseCase(repository);
    const result = await useCase.execute();
    expect(result.page).toEqual(1);
    expect(result.totalRecords).toEqual(10);
    expect(result.totalPages).toEqual(1);
    expect(result.payload.length).toEqual(10);
  });

  it('SHOULD_FIND_ALL_POSITION_PAGINATED_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const useCase = new ListPositionUseCase(repository);

    let page;
    page = await useCase.execute({ limit: 2 });
    expect(page.page).toEqual(1);
    expect(page.totalRecords).toEqual(10);
    expect(page.totalPages).toEqual(5);
    expect(page.payload.length).toEqual(2);

    const [verify] = page.payload;

    page = await useCase.execute({ limit: 2, page: 2 });
    expect(page.page).toEqual(2);
    expect(page.totalRecords).toEqual(10);
    expect(page.totalPages).toEqual(5);
    expect(page.payload.length).toEqual(2);

    expect(page.payload[0].id).not.toEqual(verify.id);
  });

  it('SHOULD_THROW_PAGE_OUT_OF_RANGE_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const useCase = new ListPositionUseCase(repository);
    try {
      await useCase.execute({ limit: 2, page: 2 });
    } catch (err) {
      expect(err.message).toEqual(`page 50 is out of range in the query`);
      expect(err.errorType).toEqual('PAGE_OUT_OF_RANGE');
      expect(err.code).toEqual(HttpStatus.BAD_REQUEST);
    }
  });

  it('SHOULD_FIND_ALL_POSITION_BY_FILTERS', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const useCase = new ListPositionUseCase(repository);
    const result = await useCase.execute({
      stringency: SearchStringency.CONTAINS,
      description: 'Encargado'
    });
    expect(result.page).toEqual(1);
    expect(result.totalRecords).toEqual(6);
    expect(result.totalPages).toEqual(1);
    expect(result.payload.length).toEqual(6);
  });

  it('SHOULD_FIND_ALL_POSITION_BY_FILTERS_V2_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const useCase = new ListPositionUseCase(repository);
    const result = await useCase.execute({
      stringency: SearchStringency.EXACTLY,
      name: 'Analista de Datos'
    });
    expect(result.page).toEqual(1);
    expect(result.totalRecords).toEqual(1);
    expect(result.totalPages).toEqual(1);
    expect(result.payload.length).toEqual(1);
  });
});
