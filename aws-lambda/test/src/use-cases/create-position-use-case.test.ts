import * as dotenv from 'dotenv';
import { DynamoDBClient, GetItemCommandOutput } from '@aws-sdk/client-dynamodb';
import { clone } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { marshall } from '@aws-sdk/util-dynamodb';
// @ts-ignore
import path from 'path';

import { MONGODB_SETTINGS_MOCK } from '../../mocks/dynamodb/MONGODB_SETTINGS.mock';
import { CreatePositionUseCase } from '../../../src/use-cases';
import { SchemaValidatorRegistry } from '../../../src/domain';
import { PositionMongoRepository } from '../../../src/ports';
import { DataBaseService } from '../../../src/services';
import { POSITION_JSON_ARRAY } from '../../mocks';
import { TestUtils } from '../../test-utils';
import { HttpStatus, Unknown } from '../../../src/types';

dotenv.config({ path: path.resolve(__dirname, '../../.env.test') });
jest.setTimeout(1000000);

describe('CREATE_POSITION_USE_CASE_SUITE_TEST', () => {
  const mockSendDDB = jest.fn();
  let mongoMemoryServer: MongoMemoryReplSet;

  beforeEach(async () => {
    jest.clearAllMocks();
    TestUtils.registerSchemasForTest();
    const { mongoServer, mongoServerHost } = await TestUtils.startMongoServerInMemory();
    MONGODB_SETTINGS_MOCK.host = mongoServerHost;
    mongoMemoryServer = mongoServer;
    DynamoDBClient.prototype.send = mockSendDDB;
  });

  afterEach(async () => {
    jest.clearAllMocks();
    jest.resetAllMocks();
    await DataBaseService.dbDisconnect();
    SchemaValidatorRegistry.registry.clear();
    await TestUtils.stopMongoServerInMemory(mongoMemoryServer);
  });

  it('SHOULD_CREATE_POSITION_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const repository = new PositionMongoRepository();
    const useCase = new CreatePositionUseCase(repository);
    const data = clone<Unknown>(POSITION_JSON_ARRAY[0]);
    const result = await useCase.execute(data);
    expect(result.success).toEqual(true);
    expect(result.message).toEqual('Position successfully saved');
    expect(result.entity.id).toMatch(/^[0-9a-fA-F]{24}$/);
  });

  it('SHOULD_THROW_ERROR_TO_TRY_CREATE_A_NEW_POSITION_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const useCase = new CreatePositionUseCase(repository);
    const data = clone<Unknown>(POSITION_JSON_ARRAY[0]);
    try {
      await useCase.execute(data);
    } catch (err) {
      expect(err.errorType).toEqual('POSITION_ALREADY_EXISTS');
      expect(err.message).toEqual(`A position already exists with the name ${data.name} and the description.`);
      expect(err.code).toEqual(HttpStatus.BAD_REQUEST);
    }
  });
});
