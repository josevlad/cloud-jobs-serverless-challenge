import * as dotenv from 'dotenv';
import { DynamoDBClient, GetItemCommandOutput } from '@aws-sdk/client-dynamodb';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { marshall } from '@aws-sdk/util-dynamodb';
// @ts-ignore
import path from 'path';

import { MONGODB_SETTINGS_MOCK } from '../../mocks/dynamodb/MONGODB_SETTINGS.mock';
import { FindOnePositionUseCase } from '../../../src/use-cases';
import { SchemaValidatorRegistry } from '../../../src/domain';
import { PositionMongoRepository } from '../../../src/ports';
import { DataBaseService } from '../../../src/services';
import { TestUtils } from '../../test-utils';
import { HttpStatus } from '../../../src/types';

dotenv.config({ path: path.resolve(__dirname, '../../.env.test') });
jest.setTimeout(1000000);

describe('FIND_ONE_POSITION_USE_CASE_SUITE_TEST', () => {
  const mockSendDDB = jest.fn();
  let mongoMemoryServer: MongoMemoryReplSet;

  beforeEach(async () => {
    jest.clearAllMocks();
    TestUtils.registerSchemasForTest();
    const { mongoServer, mongoServerHost } = await TestUtils.startMongoServerInMemory();
    MONGODB_SETTINGS_MOCK.host = mongoServerHost;
    mongoMemoryServer = mongoServer;
    DynamoDBClient.prototype.send = mockSendDDB;
  });

  afterEach(async () => {
    jest.clearAllMocks();
    jest.resetAllMocks();
    await DataBaseService.dbDisconnect();
    SchemaValidatorRegistry.registry.clear();
    await TestUtils.stopMongoServerInMemory(mongoMemoryServer);
  });

  it('SHOULD_THROW_WHEN_TRY_TO_FIND_WHIT_EMPTY_FILTERS_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const useCase = new FindOnePositionUseCase(repository);
    try {
      await useCase.execute();
    } catch (err) {
      expect(err.errorType).toEqual('NOT_FOUND');
      expect(err.message).toEqual(`A position does not exist.`);
      expect(err.code).toEqual(HttpStatus.BAD_REQUEST);
    }
  });

  it('SHOULD_FIND_AN_POSITION_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { positions } = await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const useCase = new FindOnePositionUseCase(repository);
    const result = await useCase.execute({ id: positions[0].id });
    expect(result).not.toBeNull();
  });

  it('SHOULD_FIND_AN_POSITION_USE_CASE_V2', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { positions } = await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const { name, description } = positions[0];
    const useCase = new FindOnePositionUseCase(repository);
    const result = await useCase.execute({ name, description });
    expect(result).not.toBeNull();
  });
});
