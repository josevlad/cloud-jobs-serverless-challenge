import * as dotenv from 'dotenv';
import { DynamoDBClient, GetItemCommandOutput } from '@aws-sdk/client-dynamodb';
import { clone } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { marshall } from '@aws-sdk/util-dynamodb';
// @ts-ignore
import path from 'path';

import { MONGODB_SETTINGS_MOCK } from '../../mocks/dynamodb/MONGODB_SETTINGS.mock';
import { Position, SchemaValidatorRegistry } from '../../../src/domain';
import { UpdatePositionUseCase } from '../../../src/use-cases';
import { PositionMongoRepository } from '../../../src/ports';
import { DataBaseService } from '../../../src/services';
import { TestUtils } from '../../test-utils';
import { HttpStatus } from '../../../src/types';

dotenv.config({ path: path.resolve(__dirname, '../../.env.test') });
jest.setTimeout(1000000);

describe('UPDATE_POSITION_USE_CASE_SUITE_TEST', () => {
  const mockSendDDB = jest.fn();
  let mongoMemoryServer: MongoMemoryReplSet;

  beforeEach(async () => {
    jest.clearAllMocks();
    TestUtils.registerSchemasForTest();
    const { mongoServer, mongoServerHost } = await TestUtils.startMongoServerInMemory();
    MONGODB_SETTINGS_MOCK.host = mongoServerHost;
    mongoMemoryServer = mongoServer;
    DynamoDBClient.prototype.send = mockSendDDB;
  });

  afterEach(async () => {
    jest.clearAllMocks();
    jest.resetAllMocks();
    await DataBaseService.dbDisconnect();
    SchemaValidatorRegistry.registry.clear();
    await TestUtils.stopMongoServerInMemory(mongoMemoryServer);
  });

  it('SHOULD_UPDATE_POSITION_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { positions } = await TestUtils.loadAllDataInDB();
    const inDB = positions[0];
    const toUpdate = clone<Position>(inDB);
    toUpdate.id = inDB.id.toString();
    toUpdate.name = 'DevOps';
    const repository = new PositionMongoRepository();
    const useCase = new UpdatePositionUseCase(repository);
    const result = await useCase.execute(clone(toUpdate));
    expect(result.success).toEqual(true);
    expect(result.message).toEqual('Position successfully updated');
    expect(result.entity.name).toEqual('DevOps');
    expect(result.entity.id).toEqual(inDB.id.toString());
    expect(result.entity.id).toEqual(inDB.id.toString());
  });

  it('SHOULD_THROW_ERROR_TO_TRY_UPDATE_AN_POSITION_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { positions } = await TestUtils.loadAllDataInDB();
    const toUpdate = clone<Position>(positions[0]);
    toUpdate.id = '5fcda9a6fbd0b214d0438952';
    try {
      const repository = new PositionMongoRepository();
      const useCase = new UpdatePositionUseCase(repository);
      await useCase.execute(clone(toUpdate));
    } catch (err) {
      expect(err.errorType).toEqual('NOT_FOUND');
      expect(err.message).toEqual(`A position does not exist with id 5fcda9a6fbd0b214d0438952.`);
      expect(err.code).toEqual(HttpStatus.NOT_FOUND);
    }
  });
});
