import * as dotenv from 'dotenv';
import { DynamoDBClient, GetItemCommandOutput } from '@aws-sdk/client-dynamodb';
import { clone } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { marshall } from '@aws-sdk/util-dynamodb';
// @ts-ignore
import path from 'path';

import { MONGODB_SETTINGS_MOCK } from '../../mocks/dynamodb/MONGODB_SETTINGS.mock';
import { Employee, SchemaValidatorRegistry } from '../../../src/domain';
import { UpdateEmployeeUseCase } from '../../../src/use-cases';
import { EmployeeMongoRepository } from '../../../src/ports';
import { DataBaseService } from '../../../src/services';
import { TestUtils } from '../../test-utils';
import { HttpStatus } from '../../../src/types';

dotenv.config({ path: path.resolve(__dirname, '../../.env.test') });
jest.setTimeout(1000000);

describe('UPDATE_EMPLOYEE_USE_CASE_SUITE_TEST', () => {
  const mockSendDDB = jest.fn();
  let mongoMemoryServer: MongoMemoryReplSet;

  beforeEach(async () => {
    jest.clearAllMocks();
    TestUtils.registerSchemasForTest();
    const { mongoServer, mongoServerHost } = await TestUtils.startMongoServerInMemory();
    MONGODB_SETTINGS_MOCK.host = mongoServerHost;
    mongoMemoryServer = mongoServer;
    DynamoDBClient.prototype.send = mockSendDDB;
  });

  afterEach(async () => {
    jest.clearAllMocks();
    jest.resetAllMocks();
    await DataBaseService.dbDisconnect();
    SchemaValidatorRegistry.registry.clear();
    await TestUtils.stopMongoServerInMemory(mongoMemoryServer);
  });

  it('SHOULD_UPDATE_EMPLOYEE_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    const inDB = employees[0];
    const toUpdate = clone<Employee>(inDB);
    toUpdate.id = inDB.id.toString();
    toUpdate.name = 'Tony Stark';
    toUpdate.age = 53;
    toUpdate.nationalIdentityCard = 369258142;
    const repository = new EmployeeMongoRepository();
    const useCase = new UpdateEmployeeUseCase(repository);
    const result = await useCase.execute(clone(toUpdate));
    expect(result.success).toEqual(true);
    expect(result.message).toEqual('Employee successfully updated');
    expect(result.entity.name).toEqual('Tony Stark');
    expect(result.entity.nationalIdentityCard).toEqual(369258142);
    expect(result.entity.age).toEqual(53);
    expect(result.entity.id).toEqual(inDB.id.toString());
    expect(result.entity.position.id).toEqual(inDB.position.id.toString());
  });

  it('SHOULD_UPDATE_EMPLOYEE_BY_USE_CASE_V2', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    const inDB = employees[0];
    const toUpdate = clone<Employee>(inDB);
    toUpdate.id = inDB.id.toString();
    toUpdate.name = 'Tony Stark';
    toUpdate.age = 53;
    toUpdate.nationalIdentityCard = 369258142;
    const repository = new EmployeeMongoRepository();
    const useCase = new UpdateEmployeeUseCase(repository);
    const result = await useCase.execute(clone(toUpdate));
    expect(result.success).toEqual(true);
    expect(result.message).toEqual('Employee successfully updated');
    expect(result.entity.name).toEqual('Tony Stark');
    expect(result.entity.nationalIdentityCard).toEqual(369258142);
    expect(result.entity.age).toEqual(53);
    expect(result.entity.id).toEqual(inDB.id.toString());
    expect(result.entity.position.id).toEqual(employees[0].position.id.toString());
  });

  it('SHOULD_UPDATE_EMPLOYEE_BY_USE_CASE_V3', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    const inDB = employees[0];
    const toUpdate = clone<Employee>(inDB);
    toUpdate.id = inDB.id.toString();
    toUpdate.name = 'Tony Stark';
    toUpdate.age = 53;
    toUpdate.nationalIdentityCard = 369258142;
    delete toUpdate.position;
    const repository = new EmployeeMongoRepository();
    const useCase = new UpdateEmployeeUseCase(repository);
    const result = await useCase.execute(clone(toUpdate));
    expect(result.success).toEqual(true);
    expect(result.message).toEqual('Employee successfully updated');
    expect(result.entity.name).toEqual('Tony Stark');
    expect(result.entity.nationalIdentityCard).toEqual(369258142);
    expect(result.entity.age).toEqual(53);
    expect(result.entity.id).toEqual(inDB.id.toString());
    expect(result.entity.position.id).toEqual(employees[0].position.id.toString());
  });

  it('SHOULD_THROW_ERROR_TO_TRY_UPDATE_AN_EMPLOYEE_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    const toUpdate = clone<Employee>(employees[0]);
    toUpdate.position = { id: '5fcda9a6fbd0b214d0438952', name: '', description: '' };
    try {
      const repository = new EmployeeMongoRepository();
      const useCase = new UpdateEmployeeUseCase(repository);
      await useCase.execute(clone(toUpdate));
    } catch (err) {
      expect(err.errorType).toEqual('NOT_FOUND');
      expect(err.message).toEqual(`Position ID provided does not exist in db: 5fcda9a6fbd0b214d0438952.`);
      expect(err.code).toEqual(HttpStatus.NOT_FOUND);
    }
  });
});
