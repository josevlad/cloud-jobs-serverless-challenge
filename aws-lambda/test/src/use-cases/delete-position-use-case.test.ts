import * as dotenv from 'dotenv';
import { DynamoDBClient, GetItemCommandOutput } from '@aws-sdk/client-dynamodb';
import { clone } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { marshall } from '@aws-sdk/util-dynamodb';
// @ts-ignore
import path from 'path';

import { MONGODB_SETTINGS_MOCK } from '../../mocks/dynamodb/MONGODB_SETTINGS.mock';
import { Position, SchemaValidatorRegistry } from '../../../src/domain';
import { DeletePositionUseCase } from '../../../src/use-cases';
import { PositionMongoRepository } from '../../../src/ports';
import { DataBaseService } from '../../../src/services';
import { TestUtils } from '../../test-utils';
import { HttpStatus, Unknown } from '../../../src/types';

dotenv.config({ path: path.resolve(__dirname, '../../.env.test') });
jest.setTimeout(1000000);

describe('DELETE_POSITION_USE_CASE_SUITE_TEST', () => {
  const mockSendDDB = jest.fn();
  let mongoMemoryServer: MongoMemoryReplSet;

  beforeEach(async () => {
    jest.clearAllMocks();
    TestUtils.registerSchemasForTest();
    const { mongoServer, mongoServerHost } = await TestUtils.startMongoServerInMemory();
    MONGODB_SETTINGS_MOCK.host = mongoServerHost;
    mongoMemoryServer = mongoServer;
    DynamoDBClient.prototype.send = mockSendDDB;
  });

  afterEach(async () => {
    jest.clearAllMocks();
    jest.resetAllMocks();
    await DataBaseService.dbDisconnect();
    SchemaValidatorRegistry.registry.clear();
    await TestUtils.stopMongoServerInMemory(mongoMemoryServer);
  });

  it('SHOULD_DELETE_POSITION_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { positions } = await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const useCase = new DeletePositionUseCase(repository);
    const data = clone<Unknown>(positions[0]);
    const result = await useCase.execute(data);
    expect(result.success).toEqual(true);
    expect(result.message).toEqual('Position successfully delete');
    expect(result.entity.id).toMatch(/^[0-9a-fA-F]{24}$/);
  });

  it('SHOULD_THROW_ERROR_TO_TRY_DELETE_A_NEW_POSITION_BY_USE_CASE', async () => {
    mockSendDDB.mockResolvedValueOnce({
      Item: marshall(MONGODB_SETTINGS_MOCK)
    } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { positions } = await TestUtils.loadAllDataInDB();
    const repository = new PositionMongoRepository();
    const useCase = new DeletePositionUseCase(repository);
    const position = clone<Position>(positions[0]);
    position.id = '5fcda9a6fbd0b214d0438952';
    const data = clone<Unknown>(position);
    try {
      await useCase.execute(data);
    } catch (err) {
      expect(err.errorType).toEqual('NOT_FOUND');
      expect(err.message).toEqual('A position does not exist.');
      expect(err.code).toEqual(HttpStatus.NOT_FOUND);
    }
  });
});
