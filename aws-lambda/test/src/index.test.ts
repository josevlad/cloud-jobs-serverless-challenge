import * as dotenv from 'dotenv';
import { DynamoDBClient, GetItemCommandOutput } from '@aws-sdk/client-dynamodb';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { marshall } from '@aws-sdk/util-dynamodb';
// @ts-ignore
import path from 'path';

import { POST_POSITION_EVENT_INPUT } from '../mocks/api-gateway-events-input/POST_POSITION_EVENT_INPUT.mock';
import { MONGODB_SETTINGS_MOCK } from '../mocks/dynamodb/MONGODB_SETTINGS.mock';
import { Employee, SchemaValidatorRegistry } from '../../src/domain';
import { DataBaseService } from '../../src/services';
import { SCHEMAS_VALIDATORS_MOCK } from '../mocks';
import { TestUtils } from '../test-utils';
import { Context, HttpStatus } from '../../src/types';
import { handler } from '../../index';
import { GET_POSITION_EVENT_INPUT } from '../mocks/api-gateway-events-input/GET_POSITION_EVENT_INPUT.mock';
import { GET_POSITION_ID_EVENT_INPUT } from '../mocks/api-gateway-events-input/GET_POSITION_ID_EVENT_INPUT.mock';
import { DELETE_POSITION_ID_EVENT_INPUT } from '../mocks/api-gateway-events-input/DELETE_POSITION_ID_EVENT_INPUT.mock';
import { PUT_POSITION_ID_EVENT_INPUT } from '../mocks/api-gateway-events-input/PUT_POSITION_ID_EVENT_INPUT.mock';
import { POST_EMPLOYEE_EVENT_INPUT } from '../mocks/api-gateway-events-input/POST_EMPLOYEE_EVENT_INPUT.mock';
import { EMPLOYEE_JSON_ARRAY } from '../mocks/domain/EMPLOYEE_JSON_ARRAY';
import { GET_EMPLOYEE_ID_EVENT_INPUT } from '../mocks/api-gateway-events-input/GET_EMPLOYEE_ID_EVENT_INPUT.mock';
import { DELETE_EMPLOYEE_ID_EVENT_INPUT } from '../mocks/api-gateway-events-input/DELETE_EMPLOYEE_ID_EVENT_INPUT.mock';
import { GET_EMPLOYEE_EVENT_INPUT } from '../mocks/api-gateway-events-input/GET_EMPLOYEE_EVENT_INPUT.mock';
import { PUT_EMPLOYEE_ID_EVENT_INPUT } from '../mocks/api-gateway-events-input/PUT_EMPLOYEE_ID_EVENT_INPUT.mock';
import { clone } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

dotenv.config({ path: path.resolve(__dirname, '../.env.test') });
jest.setTimeout(1000000);

describe('INDEX_HANDLER_SUITE_TEST', () => {
  const mockSendDDB = jest.fn();
  let mongoMemoryServer: MongoMemoryReplSet;

  beforeEach(async () => {
    jest.clearAllMocks();
    TestUtils.registerSchemasForTest();
    const { mongoServer, mongoServerHost } = await TestUtils.startMongoServerInMemory();
    MONGODB_SETTINGS_MOCK.host = mongoServerHost;
    mongoMemoryServer = mongoServer;
    DynamoDBClient.prototype.send = mockSendDDB;
  });

  afterEach(async () => {
    jest.clearAllMocks();
    jest.resetAllMocks();
    await DataBaseService.dbDisconnect();
    SchemaValidatorRegistry.registry.clear();
    await TestUtils.stopMongoServerInMemory(mongoMemoryServer);
  });

  it('SHOULD_CREATE_POSITION_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const { body, statusCode } = await handler(POST_POSITION_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(201);
    expect(JSON.parse(body)['success']).toEqual(true);
    expect(JSON.parse(body)['entity']['id']).toMatch(/^[0-9a-fA-F]{24}$/);
    expect(JSON.parse(body)['entity']['name']).toEqual('Gerente de Ventas');
    expect(JSON.parse(body)['entity']['description']).toEqual(
      'Encargado de liderar el equipo de ventas y alcanzar los objetivos comerciales.'
    );
  });

  it('SHOULD_THROW_ERROR_TO_TRY_CREATE_A_NEW_POSITION_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    POST_POSITION_EVENT_INPUT.body = null;
    const { body, statusCode } = await handler(POST_POSITION_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(HttpStatus.BAD_REQUEST);
    expect(JSON.parse(body)['errorType']).toEqual('SHIELD_VALIDATOR_SCHEMA_ERROR');
    expect(JSON.parse(body)['errorMessage']).toEqual('name is required');
  });

  it('SHOULD_FIND_POSITION_BY_ID_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const positions = await TestUtils.loadAllPositionInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const id = positions[2].id;
    GET_POSITION_ID_EVENT_INPUT.pathParameters = { id };
    const { body, statusCode } = await handler(GET_POSITION_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(200);
    expect(JSON.parse(body)['id']).toMatch(/^[0-9a-fA-F]{24}$/);
    expect(JSON.parse(body)['name']).toEqual(positions[2].name);
    expect(JSON.parse(body)['description']).toEqual(positions[2].description);
  });

  it('SHOULD_THROW_ERROR_TO_TRY_FIND_POSITION_BY_ID_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    GET_POSITION_ID_EVENT_INPUT.pathParameters = { id: '5fcda9a6fbd0b214d0438952' };
    const { body, statusCode } = await handler(GET_POSITION_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(HttpStatus.NOT_FOUND);
    expect(JSON.parse(body)['errorType']).toEqual('NOT_FOUND');
    expect(JSON.parse(body)['errorMessage']).toEqual('A position does not exist.');
  });

  it('SHOULD_DELETE_POSITION_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const positions = await TestUtils.loadAllPositionInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const id = positions[0].id;
    DELETE_POSITION_ID_EVENT_INPUT.pathParameters = { id };
    const { body, statusCode } = await handler(DELETE_POSITION_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(200);
    expect(JSON.parse(body)['success']).toEqual(true);
    expect(JSON.parse(body)['message']).toEqual('Position successfully delete');
    expect(JSON.parse(body)['entity']['id']).toMatch(/^[0-9a-fA-F]{24}$/);
    expect(JSON.parse(body)['entity']['name']).toEqual('Gerente de Ventas');
    expect(JSON.parse(body)['entity']['description']).toEqual(
      'Encargado de liderar el equipo de ventas y alcanzar los objetivos comerciales.'
    );
  });

  it('SHOULD_THROW_ERROR_TO_TRY_DELETE_POSITION_BY_ID_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    DELETE_POSITION_ID_EVENT_INPUT.pathParameters = { id: '5fcda9a6fbd0b214d0438952' };
    const { body, statusCode } = await handler(DELETE_POSITION_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(HttpStatus.NOT_FOUND);
    expect(JSON.parse(body)['errorType']).toEqual('NOT_FOUND');
    expect(JSON.parse(body)['errorMessage']).toEqual('A position does not exist.');
  });

  it('SHOULD_GET_LIST_POSITION_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const { body, statusCode } = await handler(GET_POSITION_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(200);
    expect(JSON.parse(body)['totalRecords']).toEqual(10);
    expect(JSON.parse(body)['totalPages']).toEqual(2);
    expect(JSON.parse(body)['payload'].length).toEqual(5);
    expect(JSON.parse(body)['limit']).toEqual(5);
    expect(JSON.parse(body)['page']).toEqual(1);
  });

  it('SHOULD_THROW_ERROR_TO_TRY_GET_LIST_POSITION_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllPositionInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    GET_POSITION_EVENT_INPUT.queryStringParameters.page = '50';
    const { body, statusCode } = await handler(GET_POSITION_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(HttpStatus.BAD_REQUEST);
    expect(JSON.parse(body)['errorType']).toEqual('PAGE_OUT_OF_RANGE');
    expect(JSON.parse(body)['errorMessage']).toEqual('page 50 is out of range in the query');
  });

  it('SHOULD_UPDATE_POSITION_BY_ID_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const positions = await TestUtils.loadAllPositionInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const id = positions[2].id;
    PUT_POSITION_ID_EVENT_INPUT.pathParameters = { id };
    PUT_POSITION_ID_EVENT_INPUT.body = JSON.stringify({ name: 'DevOps', description: 'Operador de infraestructura' });
    const { body, statusCode } = await handler(PUT_POSITION_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(200);
    expect(JSON.parse(body)['success']).toEqual(true);
    expect(JSON.parse(body)['message']).toEqual('Position successfully updated');
    expect(JSON.parse(body)['entity']['id']).toEqual(id);
    expect(JSON.parse(body)['entity']['name']).toEqual('DevOps');
    expect(JSON.parse(body)['entity']['description']).toEqual('Operador de infraestructura');
  });

  it('SHOULD_THROW_ERROR_TO_TRY_UPDATE_POSITION_BY_ID_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    PUT_POSITION_ID_EVENT_INPUT.pathParameters = { id: '5fcda9a6fbd0b214d0438952' };
    PUT_POSITION_ID_EVENT_INPUT.body = JSON.stringify({ name: 'DevOps', description: 'Operador de infraestructura' });
    const { body, statusCode } = await handler(PUT_POSITION_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(HttpStatus.NOT_FOUND);
    expect(JSON.parse(body)['errorType']).toEqual('NOT_FOUND');
    expect(JSON.parse(body)['errorMessage']).toEqual('A position does not exist with id 5fcda9a6fbd0b214d0438952.');
  });

  // ---

  it('SHOULD_CREATE_EMPLOYEE_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const positions = await TestUtils.loadAllPositionInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const reqBody = EMPLOYEE_JSON_ARRAY[0];
    const { id } = positions[0];
    POST_EMPLOYEE_EVENT_INPUT.body = JSON.stringify({ ...reqBody, ...{ position: { id } } });
    const { body, statusCode } = await handler(POST_EMPLOYEE_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(201);
    expect(JSON.parse(body)['success']).toEqual(true);
    expect(JSON.parse(body)['entity']['id']).toMatch(/^[0-9a-fA-F]{24}$/);
    expect(JSON.parse(body)['entity']['name']).toEqual('John Doe');
    expect(JSON.parse(body)['entity']['nationalIdentityCard']).toEqual(123456789);
    expect(JSON.parse(body)['entity']['age']).toEqual(30);
    expect(JSON.parse(body)['entity']['position']['id']).toMatch(/^[0-9a-fA-F]{24}$/);
  });

  it('SHOULD_THROW_ERROR_TO_TRY_CREATE_EMPLOYEE_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const positions = await TestUtils.loadAllPositionInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const { position, name } = EMPLOYEE_JSON_ARRAY[0];
    const { id } = positions[0];
    POST_EMPLOYEE_EVENT_INPUT.body = JSON.stringify({ ...{ position, name }, ...{ position: { id } } });
    const { body, statusCode } = await handler(POST_EMPLOYEE_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(HttpStatus.BAD_REQUEST);
    expect(JSON.parse(body)['errorType']).toEqual('SHIELD_VALIDATOR_SCHEMA_ERROR');
    expect(JSON.parse(body)['errorMessage']).toEqual('nationalIdentityCard is required,age is required');
  });

  it('SHOULD_FIND_EMPLOYEE_BY_ID_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const id = employees[2].id;
    GET_EMPLOYEE_ID_EVENT_INPUT.pathParameters = { id };
    const { body, statusCode } = await handler(GET_EMPLOYEE_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(200);
    expect(JSON.parse(body)['id']).toMatch(/^[0-9a-fA-F]{24}$/);
    expect(JSON.parse(body)['name']).toEqual(employees[2].name);
    expect(JSON.parse(body)['nationalIdentityCard']).toEqual(employees[2].nationalIdentityCard);
    expect(JSON.parse(body)['age']).toEqual(employees[2].age);
    expect(JSON.parse(body)['position']['id']).toEqual(employees[2].position.id);
    expect(JSON.parse(body)['position']['name']).toEqual(employees[2].position.name);
    expect(JSON.parse(body)['position']['description']).toEqual(employees[2].position.description);
  });

  it('SHOULD_THROW_ERROR_TO_TRY_FIND_EMPLOYEE_BY_ID_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    GET_EMPLOYEE_ID_EVENT_INPUT.pathParameters = { id: '5fcda9a6fbd0b214d0438952' };
    const { body, statusCode } = await handler(GET_EMPLOYEE_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(HttpStatus.NOT_FOUND);
    expect(JSON.parse(body)['errorType']).toEqual('NOT_FOUND');
    expect(JSON.parse(body)['errorMessage']).toEqual('A employee does not exist.');
    expect(JSON.parse(body)['data']['pathParameters']['id']).toEqual('5fcda9a6fbd0b214d0438952');
  });

  it('SHOULD_DELETE_EMPLOYEE_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees } = await TestUtils.loadAllDataInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const id = employees[0].id;
    DELETE_EMPLOYEE_ID_EVENT_INPUT.pathParameters = { id };
    const { body, statusCode } = await handler(DELETE_EMPLOYEE_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(200);
    expect(JSON.parse(body)['success']).toEqual(true);
    expect(JSON.parse(body)['message']).toEqual('Employee successfully delete');
    expect(JSON.parse(body)['entity']['id']).toMatch(/^[0-9a-fA-F]{24}$/);
    expect(JSON.parse(body)['entity']['name']).toEqual(employees[0].name);
    expect(JSON.parse(body)['entity']['nationalIdentityCard']).toEqual(employees[0].nationalIdentityCard);
    expect(JSON.parse(body)['entity']['age']).toEqual(employees[0].age);
  });

  it('SHOULD_THROW_ERROR_TO_TRY_DELETE_EMPLOYEE_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    DELETE_EMPLOYEE_ID_EVENT_INPUT.pathParameters = { id: '5fcda9a6fbd0b214d0438952' };
    const { body, statusCode } = await handler(DELETE_EMPLOYEE_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(HttpStatus.NOT_FOUND);
    expect(JSON.parse(body)['errorType']).toEqual('NOT_FOUND');
    expect(JSON.parse(body)['errorMessage']).toEqual('A employee does not exist.');
  });

  it('SHOULD_GET_LIST_EMPLOYEE_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const { body, statusCode } = await handler(GET_EMPLOYEE_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(200);
    expect(JSON.parse(body)['totalRecords']).toEqual(10);
    expect(JSON.parse(body)['totalPages']).toEqual(2);
    expect(JSON.parse(body)['payload'].length).toEqual(5);
    expect(JSON.parse(body)['limit']).toEqual(5);
    expect(JSON.parse(body)['page']).toEqual(1);
  });

  it('SHOULD_THROW_ERROR_TO_TRY_GET_LIST_EMPLOYEE_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    await TestUtils.loadAllDataInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    GET_EMPLOYEE_EVENT_INPUT.queryStringParameters.page = '50';
    const { body, statusCode } = await handler(GET_EMPLOYEE_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(HttpStatus.BAD_REQUEST);
    expect(JSON.parse(body)['errorType']).toEqual('PAGE_OUT_OF_RANGE');
    expect(JSON.parse(body)['errorMessage']).toEqual('page 50 is out of range in the query');
  });

  it('SHOULD_UPDATE_EMPLOYEE_BY_ID_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { employees, positions } = await TestUtils.loadAllDataInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const employee = clone<Employee>(employees[0]);
    const id = employee.id;
    PUT_EMPLOYEE_ID_EVENT_INPUT.pathParameters = { id };
    PUT_EMPLOYEE_ID_EVENT_INPUT.body = JSON.stringify({
      name: 'John Wick',
      position: {
        id: positions[7].id
      }
    });
    const { body, statusCode } = await handler(PUT_EMPLOYEE_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(200);
    expect(JSON.parse(body)['success']).toEqual(true);
    expect(JSON.parse(body)['message']).toEqual('Employee successfully updated');
    expect(JSON.parse(body)['entity']['id']).toEqual(id);
    expect(JSON.parse(body)['entity']['name']).toEqual('John Wick');
    expect(JSON.parse(body)['entity']['nationalIdentityCard']).toEqual(employee.nationalIdentityCard);
    expect(JSON.parse(body)['entity']['age']).toEqual(employee.age);
    expect(JSON.parse(body)['entity']['position']['id']).not.toEqual(employee.position.id);
    expect(JSON.parse(body)['entity']['position']['name']).not.toEqual(employees[2].position.name);
    expect(JSON.parse(body)['entity']['position']['description']).not.toEqual(employees[2].position.description);
  });

  it('SHOULD_THROW_ERROR_TO_TRY_UPDATE_EMPLOYEE_BY_ID_HANDLER', async () => {
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    await DataBaseService.connect();
    const { positions } = await TestUtils.loadAllDataInDB();
    await DataBaseService.dbDisconnect();

    mockSendDDB.mockResolvedValueOnce({ Item: marshall(MONGODB_SETTINGS_MOCK) } as unknown as GetItemCommandOutput);
    mockSendDDB.mockResolvedValueOnce({ Item: marshall(SCHEMAS_VALIDATORS_MOCK) } as unknown as GetItemCommandOutput);
    const id = '5fcda9a6fbd0b214d0438952';
    PUT_EMPLOYEE_ID_EVENT_INPUT.pathParameters = { id };
    PUT_EMPLOYEE_ID_EVENT_INPUT.body = JSON.stringify({
      name: 'John Wick',
      position: {
        id: positions[7].id
      }
    });
    const { body, statusCode } = await handler(PUT_EMPLOYEE_ID_EVENT_INPUT, {} as Context);
    expect(statusCode).toEqual(HttpStatus.NOT_FOUND);
    expect(JSON.parse(body)['errorType']).toEqual('NOT_FOUND');
    expect(JSON.parse(body)['errorMessage']).toEqual('A employee does not exist.');
  });
});
