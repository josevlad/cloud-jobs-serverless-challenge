import { AutomaticAuth } from 'mongodb-memory-server-core/lib/MongoMemoryServer';
import { MongoMemoryReplSet } from 'mongodb-memory-server';
import { Model } from 'mongoose';

import { EmployeeBuilder, PositionBuilder, SchemaValidatorRegistry } from '../../src/domain';
import { SCHEMAS_VALIDATORS_MOCK } from '../mocks';
import { EmployeeModel, PositionModel } from '../../src/ports';
import { EMPLOYEE_JSON_ARRAY } from '../mocks/domain/EMPLOYEE_JSON_ARRAY';

export class TestUtils {
  public static async startMongoServerInMemory() {
    const mongoServer = await MongoMemoryReplSet.create({
      replSet: {
        dbName: 'db-name',
        auth: {
          customRootName: 'root',
          customRootPwd: 'root'
        } as AutomaticAuth
      }
    });
    const [server] = mongoServer.servers;
    const { port, ip } = server.instanceInfo;
    return { mongoServer, mongoServerHost: `${ip}:${port}` };
  }

  public static async stopMongoServerInMemory(mongoServer: MongoMemoryReplSet) {
    await mongoServer.stop();
    await mongoServer.cleanup();
  }

  public static async loadDataInCollectionDB<T, E>(model: Model<T>, data: Array<E>) {
    await model.bulkSave(data.map((item) => new model(item)));
  }

  public static async loadAllDataInDB() {
    try {
      for (const employeeData of EMPLOYEE_JSON_ARRAY) {
        const { name, nationalIdentityCard, age, position: positionData } = employeeData;
        let existingPosition = await PositionModel.findOne({ name: positionData.name });

        if (!existingPosition) {
          existingPosition = await PositionModel.create(positionData);
        }

        const query = { name, nationalIdentityCard, age, position: existingPosition._id };

        await EmployeeModel.findOneAndUpdate(query, query, { upsert: true });
      }
      const positions = await PositionModel.find();
      const employees = await EmployeeModel.find().populate({
        path: 'position',
        model: PositionModel,
        select: 'id name description'
      });
      return {
        positions: positions.map((position) => new PositionBuilder().build(position.toObject())),
        employees: employees.map((employee) => new EmployeeBuilder().build(employee.toObject()))
      };
    } catch (error) {
      console.error('Error during bulk insertion of employees:', error);
    }
  }

  public static async loadAllPositionInDB() {
    try {
      for (const employeeData of EMPLOYEE_JSON_ARRAY) {
        const { position: positionData } = employeeData;

        let existingPosition = await PositionModel.findOne({ name: positionData.name });

        if (!existingPosition) {
          existingPosition = await PositionModel.create(positionData);
        }
      }
      const positions = await PositionModel.find();
      return positions.map((position) => new PositionBuilder().build(position.toObject()));
    } catch (error) {
      console.error('Error during bulk insertion of employees:', error);
    }
  }

  public static registerSchemasForTest(): void {
    SchemaValidatorRegistry.loadSchematicsInRegistry(SCHEMAS_VALIDATORS_MOCK);
  }

  public static generateAlphabeticString(length: number): string {
    const alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';

    for (let i = 0; i < length; i++) {
      const index = Math.floor(Math.random() * alphabet.length);
      result += alphabet.charAt(index);
    }

    return result;
  }
}
