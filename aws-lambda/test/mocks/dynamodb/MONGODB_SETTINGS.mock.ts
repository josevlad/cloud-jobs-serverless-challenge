import { SettingsIdentifiersMap } from '../../../src/services';

export const MONGODB_SETTINGS_MOCK = {
  id: SettingsIdentifiersMap.MONGODB_SETTINGS,
  user: 'root',
  pass: 'root',
  host: null,
  dbName: ''
};
