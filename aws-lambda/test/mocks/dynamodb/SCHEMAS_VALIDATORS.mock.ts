import { SettingsIdentifiersMap } from '../../../src/services';
import { SchemasValidators } from '../../../src/types';

export const SCHEMAS_VALIDATORS_MOCK: SchemasValidators = {
  id: SettingsIdentifiersMap.SCHEMAS_VALIDATORS,
  schemasValidators: [
    {
      position: {
        id: {
          pattern: '/^[0-9a-fA-F]{24}$/'
        },
        name: {
          required: true,
          pattern: '/^[a-zA-ZÀ-ÖØ-öø-ÿ0-9,.\\s:/]*$/',
          minLength: 3
        },
        description: {
          pattern: '/^[a-zA-ZÀ-ÖØ-öø-ÿ0-9,.\\s:]*$/',
          maxLength: 100
        }
      }
    },
    {
      employee: {
        id: {
          pattern: '/^[0-9a-fA-F]{24}$/'
        },
        name: {
          required: true,
          pattern: '/^[a-zA-ZÀ-ÖØ-öø-ÿ,.\\s:]*$/',
          minLength: 3
        },
        nationalIdentityCard: {
          required: true,
          onlyNumbers: true
        },
        age: {
          required: true,
          onlyNumbers: true,
          min: 18,
          max: 65
        },
        position: {
          required: true
        }
      }
    }
  ]
};
