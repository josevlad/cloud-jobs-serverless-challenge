export const POSITION_JSON_ARRAY = [
  {
    name: 'Gerente de Ventas',
    description: 'Encargado de liderar el equipo de ventas y alcanzar los objetivos comerciales.'
  },
  {
    name: 'Desarrollador Full Stack',
    description: 'Responsable del desarrollo tanto del frontend como del backend de aplicaciones web.'
  },
  {
    name: 'Especialista en Marketing Digital',
    description: 'Encargado de diseñar estrategias de marketing en línea para aumentar la visibilidad de la marca.'
  },
  {
    name: 'Analista de Datos',
    description: 'Responsable de analizar conjuntos de datos para obtener información relevante y tomar decisiones.'
  },
  {
    name: 'Diseñador UX/UI',
    description: 'Encargado de crear interfaces intuitivas y atractivas para mejorar la experiencia del usuario.'
  },
  {
    name: 'Coordinador de Proyectos',
    description: 'Encargado de planificar, ejecutar y supervisar proyectos para lograr objetivos específicos.'
  },
  {
    name: 'Especialista en Recursos Humanos',
    description: 'Responsable de la gestión del talento, reclutamiento y desarrollo del personal.'
  },
  {
    name: 'Ingeniero de Software',
    description: 'Encargado del diseño, desarrollo y mantenimiento de aplicaciones informáticas.'
  },
  {
    name: 'Analista Financiero',
    description: 'Responsable del análisis de datos financieros para la toma de decisiones estratégicas.'
  },
  {
    name: 'Especialista en Soporte Técnico',
    description: 'Encargado de brindar asistencia técnica para resolver problemas informáticos.'
  }
];
