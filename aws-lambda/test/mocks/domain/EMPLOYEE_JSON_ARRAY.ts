import { POSITION_JSON_ARRAY } from './POSITION_JSON_ARRAY';

export const EMPLOYEE_JSON_ARRAY = [
  {
    name: 'John Doe',
    nationalIdentityCard: 123456789,
    age: 30,
    position: POSITION_JSON_ARRAY[0]
  },
  {
    name: 'Jane Smith',
    nationalIdentityCard: 987654321,
    age: 25,
    position: POSITION_JSON_ARRAY[1]
  },
  {
    name: 'Michael Johnson',
    nationalIdentityCard: 456789123,
    age: 35,
    position: POSITION_JSON_ARRAY[2]
  },
  {
    name: 'Emily Davis',
    nationalIdentityCard: 789123456,
    age: 28,
    position: POSITION_JSON_ARRAY[3]
  },
  {
    name: 'David Wilson',
    nationalIdentityCard: 321654987,
    age: 40,
    position: POSITION_JSON_ARRAY[4]
  },
  {
    name: 'Sophia Garcia',
    nationalIdentityCard: 654987321,
    age: 22,
    position: POSITION_JSON_ARRAY[5]
  },
  {
    name: 'Daniel Martinez',
    nationalIdentityCard: 159753468,
    age: 33,
    position: POSITION_JSON_ARRAY[6]
  },
  {
    name: 'Olivia Brown',
    nationalIdentityCard: 852963741,
    age: 29,
    position: POSITION_JSON_ARRAY[7]
  },
  {
    name: 'Liam Miller',
    nationalIdentityCard: 369258147,
    age: 27,
    position: POSITION_JSON_ARRAY[8]
  },
  {
    name: 'Ava Lee',
    nationalIdentityCard: 741852963,
    age: 31,
    position: POSITION_JSON_ARRAY[9]
  }
];
