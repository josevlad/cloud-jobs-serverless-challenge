import { EndpointsMap } from '../../../src/adapters';
import { EVENE_BASE } from './EVENE_BASE.mock';
import { constants } from 'http2';

export const GET_EMPLOYEE_ID_EVENT_INPUT = {
  ...EVENE_BASE,
  ...{
    httpMethod: constants.HTTP2_METHOD_GET,
    resource: EndpointsMap.EMPLOYEE_ID_PATH
  }
};
