import { POSITION_JSON_ARRAY } from '../domain/POSITION_JSON_ARRAY';
import { EndpointsMap } from '../../../src/adapters';
import { EVENE_BASE } from './EVENE_BASE.mock';
import { constants } from 'http2';

export const POST_POSITION_EVENT_INPUT = {
  ...EVENE_BASE,
  ...{
    body: JSON.stringify(POSITION_JSON_ARRAY[0]),
    httpMethod: constants.HTTP2_METHOD_POST,
    resource: EndpointsMap.POSITION_PATH
  }
};
