import { EndpointsMap } from '../../../src/adapters';
import { EVENE_BASE } from './EVENE_BASE.mock';
import { constants } from 'http2';

export const POST_EMPLOYEE_EVENT_INPUT = {
  ...EVENE_BASE,
  ...{
    httpMethod: constants.HTTP2_METHOD_POST,
    resource: EndpointsMap.EMPLOYEE_PATH
  }
};
