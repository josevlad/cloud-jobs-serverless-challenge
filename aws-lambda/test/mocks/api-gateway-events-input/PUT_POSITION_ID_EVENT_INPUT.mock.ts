import { EndpointsMap } from '../../../src/adapters';
import { EVENE_BASE } from './EVENE_BASE.mock';
import { constants } from 'http2';

export const PUT_POSITION_ID_EVENT_INPUT = {
  ...EVENE_BASE,
  ...{
    httpMethod: constants.HTTP2_METHOD_PUT,
    resource: EndpointsMap.POSITION_ID_PATH
  }
};
