import { APIGatewayEvent } from '../../../src/types';

export const EVENE_BASE: APIGatewayEvent = {
  resource: '',
  path: '',
  httpMethod: '',
  body: null,
  headers: {
    accept: 'application/json, text/plain, */*',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'es-US,es-419;q=0.9,es;q=0.8,en;q=0.7',
    Host: 'ccr-moncap-capacity-manager-api.ecomm-stg.cencosud.com',
    origin: 'http://localhost:8080',
    referer: 'http://localhost:8080/',
    'sec-ch-ua': '"Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"macOS"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'cross-site',
    'sec-gpc': '1',
    'User-Agent':
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36',
    'X-Amzn-Trace-Id': 'Root=1-656e34a8-55b09bad122e937b0d1b9405',
    'X-Forwarded-For': '186.122.180.82',
    'X-Forwarded-Port': '443',
    'X-Forwarded-Proto': 'https'
  },
  multiValueHeaders: {
    accept: ['application/json, text/plain, */*'],
    'accept-encoding': ['gzip, deflate, br'],
    'accept-language': ['es-US,es-419;q=0.9,es;q=0.8,en;q=0.7'],
    Host: ['ccr-moncap-capacity-manager-api.ecomm-stg.cencosud.com'],
    origin: ['http://localhost:8080'],
    referer: ['http://localhost:8080/'],
    'sec-ch-ua': ['"Google Chrome";v="119", "Chromium";v="119", "Not?A_Brand";v="24"'],
    'sec-ch-ua-mobile': ['?0'],
    'sec-ch-ua-platform': ['"macOS"'],
    'sec-fetch-dest': ['empty'],
    'sec-fetch-mode': ['cors'],
    'sec-fetch-site': ['cross-site'],
    'sec-gpc': ['1'],
    'User-Agent': [
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'
    ],
    'X-Amzn-Trace-Id': ['Root=1-656e34a8-55b09bad122e937b0d1b9405'],
    'X-Forwarded-For': ['186.122.180.82'],
    'X-Forwarded-Port': ['443'],
    'X-Forwarded-Proto': ['https']
  },
  queryStringParameters: null, // { limit: '8', page: '1', populate: 'true' }
  multiValueQueryStringParameters: { limit: ['8'], page: ['1'], populate: ['true'] },
  pathParameters: null, // { country: 'PE', flag: 'WONG' }
  stageVariables: { deployed_at: '2023-12-01T14:08:09Z' },
  requestContext: {
    resourceId: 'gyxziu',
    resourcePath: '/v2/{country}/{flag}/store',
    httpMethod: 'GET',
    extendedRequestId: 'Pb0qcF8uIAMEkSw=',
    requestTime: '04/Dec/2023:20:20:56 +0000',
    path: '/v2/PE/WONG/store',
    accountId: '116948010203',
    protocol: 'HTTP/1.1',
    stage: 'dev',
    domainPrefix: 'ccr-moncap-capacity-manager-api',
    requestTimeEpoch: 1701721256902,
    requestId: '222642d4-0ba1-4dba-b927-667d32960993',
    identity: {
      cognitoIdentityPoolId: null,
      accountId: null,
      cognitoIdentityId: null,
      caller: null,
      sourceIp: '186.122.180.82',
      principalOrgId: null,
      accessKey: null,
      cognitoAuthenticationType: null,
      cognitoAuthenticationProvider: null,
      userArn: null,
      userAgent:
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36',
      user: null,
      apiKey: '',
      apiKeyId: '',
      clientCert: undefined
    },
    domainName: '',
    apiId: '5737baam0e',
    authorizer: {}
  },
  isBase64Encoded: false
};
