import { BusinessException, clone, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { IUseCase } from './interfaces/use-case.interface';
import { HttpStatus, Unknown } from '../types';
import { IRepository } from '../ports';
import { Employee } from '../domain';

export class FindOneEmployeeUseCase implements IUseCase<Employee> {
  constructor(private repository: IRepository<Employee>) {}

  public async execute(data?: Unknown): Promise<Employee> {
    console.debug('LIST_EMPLOYEE_USE_CASE.EXECUTE', { data });
    const filters = !isEmpty(data) ? clone<Partial<Employee>>(data) : {};
    const entityInDB = await this.repository.findOneByFilters(filters);
    if (isEmpty(entityInDB))
      throw new BusinessException({
        errorMessage: `A employee does not exist.`,
        errorType: 'NOT_FOUND',
        code: HttpStatus.NOT_FOUND
      });
    return entityInDB;
  }
}
