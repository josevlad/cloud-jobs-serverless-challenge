import { BusinessException, clone, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { IUseCase } from './interfaces/use-case.interface';
import { HttpStatus, Unknown } from '../types';
import { IRepository } from '../ports';
import { Position } from '../domain';

export class FindOnePositionUseCase implements IUseCase<Position> {
  constructor(private repository: IRepository<Position>) {}

  public async execute(data?: Unknown): Promise<Position> {
    console.debug('LIST_EMPLOYEE_USE_CASE.EXECUTE', { data });
    const filters = !isEmpty(data) ? clone<Partial<Position>>(data) : {};
    const entityInDB = await this.repository.findOneByFilters(filters);
    if (isEmpty(entityInDB))
      throw new BusinessException({
        errorMessage: `A position does not exist.`,
        errorType: 'NOT_FOUND',
        code: HttpStatus.NOT_FOUND
      });
    return entityInDB;
  }
}
