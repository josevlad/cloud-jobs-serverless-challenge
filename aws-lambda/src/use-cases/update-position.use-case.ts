import { BusinessException, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { ActionResult, HttpStatus, Unknown } from '../types';
import { IUseCase } from './interfaces/use-case.interface';
import { Position, PositionBuilder } from '../domain';
import { IRepository } from '../ports';

export class UpdatePositionUseCase implements IUseCase<ActionResult<Position>> {
  constructor(private repository: IRepository<Position>) {}

  public async execute(data?: Unknown): Promise<ActionResult<Position>> {
    console.debug('CREATE_EMPLOYEE_USE_CASE.EXECUTE', { data });
    const entity = new PositionBuilder().build(data);
    const { id } = entity;
    const entityInDB = await this.repository.findOneByFilters({ id });
    if (isEmpty(entityInDB))
      throw new BusinessException({
        errorMessage: `A position does not exist with id ${id}.`,
        errorType: 'NOT_FOUND',
        code: HttpStatus.NOT_FOUND
      });

    return await this.repository.update(entity);
  }
}
