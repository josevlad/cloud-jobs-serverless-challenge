import { IUseCase } from './interfaces/use-case.interface';
import { Page, SearchFilters, SearchStringency, Unknown } from '../types';
import { Employee } from '../domain';
import { IRepository } from '../ports';
import { clone, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

export class ListEmployeeUseCase implements IUseCase<Page<Array<Employee>>> {
  constructor(private repository: IRepository<Employee>) {}

  public async execute(data?: Unknown): Promise<Page<Array<Employee>>> {
    console.debug('LIST_EMPLOYEE_USE_CASE.EXECUTE', { data });
    const filters = !isEmpty(data) ? clone<SearchFilters<{ stringency: SearchStringency } & Employee>>(data) : {};
    return await this.repository.findAll(filters);
  }
}
