import { BusinessException, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { ActionResult, HttpStatus, Unknown } from '../types';
import { IUseCase } from './interfaces/use-case.interface';
import { Position, PositionBuilder } from '../domain';
import { IRepository } from '../ports';

export class CreatePositionUseCase implements IUseCase<ActionResult<Position>> {
  constructor(private repository: IRepository<Position>) {}

  public async execute(data?: Unknown): Promise<ActionResult<Position>> {
    console.debug('CREATE_POSITION_USE_CASE.EXECUTE', { data });
    const entity = new PositionBuilder().build(data);
    const { name, description } = entity;
    const positionInDB = await this.repository.findOneByFilters({ name, description });
    if (!isEmpty(positionInDB))
      throw new BusinessException({
        errorMessage: `A position already exists with the name ${name} and the description.`,
        errorType: 'POSITION_ALREADY_EXISTS',
        code: HttpStatus.BAD_REQUEST
      });

    return await this.repository.save(entity);
  }
}
