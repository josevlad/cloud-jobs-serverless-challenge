import { Unknown } from '../../types';

export interface IUseCase<Entity> {
  execute(data?: Unknown): Promise<Entity>;
}
