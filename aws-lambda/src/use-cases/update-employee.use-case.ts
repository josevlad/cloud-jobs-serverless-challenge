import { BusinessException, clone, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { ActionResult, HttpStatus, Unknown } from '../types';
import { IUseCase } from './interfaces/use-case.interface';
import { IRepository } from '../ports';
import { Employee } from '../domain';

export class UpdateEmployeeUseCase implements IUseCase<ActionResult<Employee>> {
  constructor(private repository: IRepository<Employee>) {}

  public async execute(data?: Unknown): Promise<ActionResult<Employee>> {
    console.debug('CREATE_EMPLOYEE_USE_CASE.EXECUTE', { data });
    const entity = clone<Partial<Employee>>(data);
    const { id, nationalIdentityCard } = entity;
    const entityInDB = await this.repository.findOneByFilters({ id, nationalIdentityCard });
    if (isEmpty(entityInDB))
      throw new BusinessException({
        errorMessage: `A employee does not exist.`,
        errorType: 'NOT_FOUND',
        code: HttpStatus.NOT_FOUND
      });

    return await this.repository.update(entity);
  }
}
