import { IUseCase } from './interfaces/use-case.interface';
import { Page, SearchFilters, SearchStringency, Unknown } from '../types';
import { Employee, Position } from '../domain';
import { IRepository } from '../ports';
import { clone, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

export class ListPositionUseCase implements IUseCase<Page<Array<Position>>> {
  constructor(private repository: IRepository<Position>) {}

  public async execute(data?: Unknown): Promise<Page<Array<Position>>> {
    console.debug('LIST_POSITION_USE_CASE.EXECUTE', { data });
    const filters = !isEmpty(data) ? clone<SearchFilters<{ stringency: SearchStringency } & Employee>>(data) : {};
    return await this.repository.findAll(filters);
  }
}
