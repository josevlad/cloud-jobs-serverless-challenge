import { BusinessException, clone, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { ActionResult, HttpStatus, Unknown } from '../types';
import { IUseCase } from './interfaces/use-case.interface';
import { Position } from '../domain';
import { IRepository } from '../ports';

export class DeletePositionUseCase implements IUseCase<ActionResult<Position>> {
  constructor(private repository: IRepository<Position>) {}

  public async execute(data?: Unknown): Promise<ActionResult<Position>> {
    console.debug('CREATE_EMPLOYEE_USE_CASE.EXECUTE', { data });
    const entity = clone<Partial<Position>>(data);
    const { id } = entity;
    const entityInDB = await this.repository.findOneByFilters({ id });
    if (isEmpty(entityInDB))
      throw new BusinessException({
        errorMessage: `A position does not exist.`,
        errorType: 'NOT_FOUND',
        code: HttpStatus.NOT_FOUND
      });

    return await this.repository.delete(id);
  }
}
