import { BusinessException, clone, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { ActionResult, HttpStatus, Unknown } from '../types';
import { IUseCase } from './interfaces/use-case.interface';
import { Employee, EmployeeBuilder } from '../domain';
import { IRepository, PositionMongoRepository } from '../ports';

export class CreateEmployeeUseCase implements IUseCase<ActionResult<Employee>> {
  private positionMongoRepository: PositionMongoRepository;

  constructor(private repository: IRepository<Employee>) {
    this.positionMongoRepository = new PositionMongoRepository();
  }

  public async execute(data?: Unknown): Promise<ActionResult<Employee>> {
    console.debug('CREATE_EMPLOYEE_USE_CASE.EXECUTE', { data });
    const {
      position: { id }
    } = clone<Employee>(data);
    const position = await this.positionMongoRepository.findOneByFilters({ id });

    if (isEmpty(position))
      throw new BusinessException({
        errorMessage: `A position does not exist with id: ${id}.`,
        errorType: 'NOT_FOUND',
        code: HttpStatus.NOT_FOUND
      });

    const entity = new EmployeeBuilder().build({ ...data, ...{ position } });
    const { nationalIdentityCard } = entity;
    const entityInDB = await this.repository.findOneByFilters({ nationalIdentityCard });
    if (!isEmpty(entityInDB))
      throw new BusinessException({
        errorMessage: `A employee already exists with the nationalIdentityCard ${nationalIdentityCard}.`,
        errorType: 'EMPLOYEE_ALREADY_EXISTS',
        code: HttpStatus.BAD_REQUEST
      });

    return await this.repository.save(entity);
  }
}
