import { Position } from './position';

export class Employee {
  constructor(
    public id: string,
    public name: string,
    public nationalIdentityCard: number,
    public age: number,
    public position: Position
  ) {}
}
