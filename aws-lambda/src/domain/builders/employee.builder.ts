import { BusinessException, isEmpty, ShieldValidator } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { SchemasValidatorsIdentifiersMap } from './schemas-validators-identifiers.map';
import { SchemaValidatorRegistry } from './schema-validator.registry';
import { PositionBuilder } from './position.builder';
import { Employee } from '../entities/employee';
import { Position } from '../entities/position';
import { Builder } from './builder.interface';
import { HttpStatus, Unknown } from '../../types';

export class EmployeeBuilder implements Builder<Employee> {
  private id: string = undefined;
  private name: string = undefined;
  private nationalIdentityCard: number = undefined;
  private age: number = undefined;
  private position: Position = undefined;

  public setId(value: string) {
    this.id = value;
    return this;
  }

  public setName(value: string) {
    this.name = value;
    return this;
  }

  public setNationalIdentityCard(value: number) {
    this.nationalIdentityCard = value;
    return this;
  }

  public setAge(value: number) {
    this.age = value;
    return this;
  }

  public setPosition(value: Position) {
    this.position = value;
    return this;
  }

  build(json?: Unknown): Employee {
    if (!isEmpty(json)) {
      Object.keys(json).forEach((key) => {
        if (key === 'position') {
          const position = new PositionBuilder().build(json[key] as Unknown);
          this[`set${key.charAt(0).toUpperCase() + key.slice(1)}`](position);
        } else {
          this[`set${key.charAt(0).toUpperCase() + key.slice(1)}`](json[key]);
        }
      });
    }
    const employee = new Employee(this.id, this.name, this.nationalIdentityCard, this.age, this.position);
    const { isValid, errors } = ShieldValidator.validate(
      employee,
      SchemaValidatorRegistry.registry.get(SchemasValidatorsIdentifiersMap.EMPLOYEE)
    );
    if (!isValid) {
      throw new BusinessException({
        errorMessage: errors.toString(),
        errorType: ShieldValidator.SHIELD_VALIDATOR_ERROR_TAG,
        code: HttpStatus.BAD_REQUEST
      });
    }
    return employee;
  }
}
