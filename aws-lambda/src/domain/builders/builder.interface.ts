import { Unknown } from '../../types';

export interface Builder<T> {
  build(json?: Unknown): T;
}
