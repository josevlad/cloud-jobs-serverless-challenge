import { BusinessException, isEmpty, ShieldValidator } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { SchemaValidatorRegistry } from './schema-validator.registry';
import { SchemasValidatorsIdentifiersMap } from './schemas-validators-identifiers.map';
import { Position } from '../entities/position';
import { Builder } from './builder.interface';
import { HttpStatus, Unknown } from '../../types';

export class PositionBuilder implements Builder<Position> {
  private id: string = undefined;
  private name: string = undefined;
  private description: string = undefined;

  public setId(value: string) {
    this.id = value;
    return this;
  }

  public setName(value: string) {
    this.name = value;
    return this;
  }

  public setDescription(value: string) {
    this.description = value;
    return this;
  }

  build(json?: Unknown): Position {
    if (!isEmpty(json)) {
      Object.keys(json).forEach((key) => {
        this[`set${key.charAt(0).toUpperCase() + key.slice(1)}`](json[key]);
      });
    }
    const position = new Position(this.id, this.name, this.description);
    const { isValid, errors } = ShieldValidator.validate(
      position,
      SchemaValidatorRegistry.registry.get(SchemasValidatorsIdentifiersMap.POSITION)
    );
    if (!isValid) {
      throw new BusinessException({
        errorMessage: errors.toString(),
        errorType: ShieldValidator.SHIELD_VALIDATOR_ERROR_TAG,
        code: HttpStatus.BAD_REQUEST
      });
    }
    return position;
  }
}
