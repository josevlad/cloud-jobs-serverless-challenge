import { SchemaValidatorType } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { ConfigurationService, SettingsIdentifiersMap } from '../../services';
import { SchemasValidators } from '../../types';

export class SchemaValidatorRegistry {
  public static registry: Map<string, SchemaValidatorType> = new Map<string, SchemaValidatorType>();

  public static async registerSchemas() {
    const { SCHEMAS_VALIDATORS } = SettingsIdentifiersMap;
    const schemasValidators = await ConfigurationService.getSettingsByIdFromDDB<SchemasValidators>(SCHEMAS_VALIDATORS);
    this.loadSchematicsInRegistry(schemasValidators);
  }

  public static loadSchematicsInRegistry({ schemasValidators }: SchemasValidators): void {
    this.registry.clear();
    schemasValidators.forEach((schema) => {
      const key = Object.keys(schema)[0];
      const value = schema[key];
      const convertedValue = Object.entries(value).reduce((schemaValidator, [prop, propValue]) => {
        schemaValidator[prop] =
          'pattern' in propValue
            ? { ...propValue, pattern: new RegExp((propValue['pattern'] as string).slice(1, -1)) }
            : propValue;
        return schemaValidator;
      }, {});
      this.registry.set(key, convertedValue as SchemaValidatorType);
    });
  }
}
