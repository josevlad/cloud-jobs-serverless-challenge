import { BusinessException, getEnv, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { DynamoDBClient, GetItemCommand, GetItemCommandInput } from '@aws-sdk/client-dynamodb';
import { marshall, unmarshall } from '@aws-sdk/util-dynamodb';
import { HttpStatus } from '../types';

export class ConfigurationService {
  private static dbClient: DynamoDBClient;
  private static table: string;

  private static build() {
    this.table = isEmpty(this.table) ? getEnv('VJC_DYNAMO_BACK_CONFIGURATIONS', String) : this.table;
    this.dbClient = isEmpty(this.dbClient) ? new DynamoDBClient({ region: 'us-east-1' }) : (this.dbClient as DynamoDBClient);
  }

  private static getObjectGetItemCommandInput(id: string): GetItemCommand {
    const command: GetItemCommandInput = {
      TableName: this.table,
      Key: marshall({ id: id })
    };
    return new GetItemCommand(command);
  }

  public static async getSettingsByIdFromDDB<T>(settingId: string): Promise<T> {
    this.build();
    try {
      const commandInput = this.getObjectGetItemCommandInput(settingId);
      const getItemCommandOutput = await this.dbClient.send(commandInput);
      return unmarshall(getItemCommandOutput?.Item) as T;
    } catch (err) {
      console.error('CONFIGURATION_SERVICE.GET_SETTINGS_BY_ID_FROM_DDB.ERROR: ', { err, message: err.message });
      throw new BusinessException({
        errorMessage: `Error Getting document ${settingId}: ${err.message}`,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
  }
}
