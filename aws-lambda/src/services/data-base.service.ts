import { BusinessException, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import mongoose, { Mongoose } from 'mongoose';

import { ConfigurationService } from './configuration.service';
import { SettingsIdentifiersMap } from './settings-identifiers.map';
import { HttpStatus } from '../types';

let connection: Mongoose;

export class DataBaseService {
  public static async connect(): Promise<Mongoose> {
    const { MONGODB_SETTINGS } = SettingsIdentifiersMap;
    if (isEmpty(connection)) {
      try {
        const { user, pass, host, dbName } = await ConfigurationService.getSettingsByIdFromDDB<MongodbSettings>(MONGODB_SETTINGS);
        connection = await mongoose.connect(
          `mongodb://${user}:${pass}@${host}/${dbName}?authMechanism=DEFAULT&retryWrites=false`
        );
      } catch (err) {
        console.error('DATA_BASE_SERVICE.CONNECT.ERROR', { err, message: err.message });
        throw new BusinessException({
          errorType: 'An unexpected error occurred, please try again or report the issue to the development team.',
          errorMessage: String(err.message),
          code: HttpStatus.INTERNAL_SERVER_ERROR
        });
      }
      console.debug('DATA_BASE_SERVICE.CONNECT', 'SUCCESSFUL CONNECTION');
      return connection;
    }
  }

  public static async dbDisconnect() {
    await mongoose.disconnect();
    connection = null;
  }
}

type MongodbSettings = {
  id: string;
  user: string;
  pass: string;
  host: string;
  dbName: string;
};
