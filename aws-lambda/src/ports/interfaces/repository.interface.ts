import { ActionResult, Page, SearchFilters, SearchStringency, Unknown } from '../../types';

export interface IRepository<Entity> {
  save(entity: Omit<Entity, 'id'>): Promise<ActionResult<Entity>>;

  findOneByFilters(filters: Partial<Entity>): Promise<Entity | null>;

  update(entity: Partial<Entity>): Promise<ActionResult<Entity>>;

  findAll(filters?: SearchFilters<{ stringency: SearchStringency } & Entity>): Promise<Page<Array<Entity> | Array<never>>>;

  delete(id: string): Promise<ActionResult<Entity>>;

  mapToDomain(data: Unknown): Entity;
}
