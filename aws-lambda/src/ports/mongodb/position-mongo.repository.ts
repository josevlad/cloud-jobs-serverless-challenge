import { BusinessException, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { Types } from 'mongoose';

import { ActionResult, HttpStatus, Page, SearchFilters, SearchStringency, Unknown } from '../../types';
import { IRepository } from '../interfaces/repository.interface';
import { PositionModel } from './models/position.model';
import { Position, PositionBuilder } from '../../domain';
import { MongoUtils } from './mongo.utils';

export class PositionMongoRepository implements IRepository<Position> {
  public async save(entity: Omit<Position, 'id'>): Promise<ActionResult<Position>> {
    console.debug('POSITION_MONGO_REPOSITORY.SAVE', { entity });
    try {
      const { name, description } = entity;
      const result = await PositionModel.create({ name, description });
      return {
        success: true,
        message: 'Position successfully saved',
        entity: this.mapToDomain(result.toObject())
      };
    } catch (err) {
      console.error('POSITION_MONGO_REPOSITORY.SAVE.ERROR', { err, message: err.message, entity });
      throw new BusinessException({
        errorMessage: err.message,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
  }

  public async findOneByFilters(filters: Partial<Position>): Promise<Position | null> {
    console.debug('POSITION_MONGO_REPOSITORY.FIND_ONE_BY_FILTERS', { filters });
    try {
      const { id, ...otherFilters } = filters;
      const searchCriteria = isEmpty(id) ? otherFilters : { _id: Types.ObjectId.createFromHexString(id) };
      const hasExist = await PositionModel.findOne(searchCriteria);
      return isEmpty(hasExist) ? null : this.mapToDomain(hasExist.toObject());
    } catch (err) {
      console.error('POSITION_MONGO_REPOSITORY.FIND_ONE_BY_FILTERS.ERROR', { err, message: err.message, filters });
      throw new BusinessException({
        errorMessage: err.message,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
  }

  public async update(entity: Partial<Position>): Promise<ActionResult<Position>> {
    console.debug('POSITION_MONGO_REPOSITORY.UPDATE', { entity });
    try {
      const { id, ...data } = entity;
      const result = await PositionModel.updateOne({ _id: Types.ObjectId.createFromHexString(id) }, { $set: data });
      return {
        success: result.modifiedCount === 1,
        message: 'Position successfully updated',
        entity: entity
      };
    } catch (err) {
      console.error('POSITION_MONGO_REPOSITORY.UPDATE.ERROR', { err, message: err.message, entity });
      throw new BusinessException({
        errorMessage: err.message,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
  }

  public async findAll(
    filters?: SearchFilters<{ stringency: SearchStringency } & Position>
  ): Promise<Page<Array<Position> | Array<never>>> {
    console.debug('POSITION_MONGO_REPOSITORY.FIND_ALL', { filters });
    let { stringency = SearchStringency.EXACTLY, page = 1, limit = 20, ...otherProperties } = filters ?? {};
    page = parseInt(page as unknown as string);
    limit = parseInt(limit as unknown as string);
    const query = otherProperties;

    Object.keys(query).forEach((key) => {
      if (!isEmpty(query[key]))
        query[key] =
          stringency === SearchStringency.EXACTLY
            ? (query[key] as string)
            : ({ $regex: query[key], $options: 'i' } as { $regex: string; $options: string });
    });

    const totalRecords = await PositionModel.countDocuments(query);
    MongoUtils.verifyPageRange(page, limit, totalRecords);
    try {
      const totalPages = Math.ceil(totalRecords / limit);
      const docs = await PositionModel.find(query)
        .limit(limit)
        .skip((page - 1) * limit)
        .exec();
      const payload = isEmpty(docs) ? [] : docs.map((doc) => this.mapToDomain(doc.toObject()));

      return { totalRecords, totalPages, payload, limit, page };
    } catch (err) {
      console.error('POSITION_MONGO_REPOSITORY.FIND_ALL.ERROR', { err, message: err.message, filters });
      throw new BusinessException({
        errorMessage: err.message,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
  }

  public async delete(id: string): Promise<ActionResult<Position>> {
    console.debug('POSITION_MONGO_REPOSITORY.DELETE', { id });
    let result = null;
    try {
      result = await PositionModel.findByIdAndDelete({ _id: Types.ObjectId.createFromHexString(id) });
    } catch (err) {
      console.error('POSITION_MONGO_REPOSITORY.SAVE.DELETE', { err, message: err.message, id });
      throw new BusinessException({
        errorMessage: err.message,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
    if (isEmpty(result))
      throw new BusinessException({
        errorMessage: `record not found with id: ${id}`,
        errorType: 'NOT_FOUND',
        code: HttpStatus.NOT_FOUND
      });

    return {
      success: true,
      message: 'Position successfully delete',
      entity: this.mapToDomain(result.toObject())
    };
  }

  mapToDomain(data: Unknown): Position {
    return new PositionBuilder().build(data);
  }
}
