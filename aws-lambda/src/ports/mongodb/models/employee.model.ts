import mongoose, { Model } from 'mongoose';

import { EmployeeDocument } from '../documents/employee.document';
import { EmployeeSchema } from '../schemes/employee.schema';

export const EmployeeModel: Model<EmployeeDocument> = mongoose.model('Employee', EmployeeSchema);
