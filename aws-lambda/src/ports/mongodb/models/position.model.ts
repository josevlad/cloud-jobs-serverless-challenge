import mongoose, { Model } from 'mongoose';

import { PositionSchema } from '../schemes/position.schema';
import { PositionDocument } from '../documents/position.document';

export const PositionModel: Model<PositionDocument> = mongoose.model('Positio', PositionSchema);
