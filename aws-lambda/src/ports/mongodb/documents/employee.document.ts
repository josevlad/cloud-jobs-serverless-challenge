import { Document } from 'mongoose';
import { Employee } from '../../../domain';

export interface EmployeeDocument extends Document, Omit<Employee, 'id'> {}
