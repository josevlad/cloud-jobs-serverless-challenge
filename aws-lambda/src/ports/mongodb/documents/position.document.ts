import { Document } from 'mongoose';
import { Position } from '../../../domain';

export interface PositionDocument extends Document, Omit<Position, 'id'> {}
