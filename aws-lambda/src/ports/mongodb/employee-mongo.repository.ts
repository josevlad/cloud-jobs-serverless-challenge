import { BusinessException, isEmpty } from '@cencosud-ds/capacity-cross-architecture-ts-utils';

import { ActionResult, HttpStatus, Page, SearchFilters, SearchStringency, Unknown } from '../../types';
import { PositionMongoRepository } from './position-mongo.repository';
import { IRepository } from '../interfaces/repository.interface';
import { EmployeeModel } from './models/employee.model';
import { Employee, EmployeeBuilder, Position } from '../../domain';
import { PositionModel } from './models/position.model';
import { Types } from 'mongoose';
import { MongoUtils } from './mongo.utils';

export class EmployeeMongoRepository implements IRepository<Employee> {
  private positionMongoRepository: IRepository<Position> = new PositionMongoRepository();

  public async save(entity: Omit<Employee, 'id'>): Promise<ActionResult<Employee>> {
    console.debug('EMPLOYEE_MONGO_REPOSITORY.SAVE', { entity });
    const {
      position: { id }
    } = entity;
    const positionInDB = await this.getPositionByID(id);
    try {
      const created = await EmployeeModel.create({ ...entity, ...{ position: positionInDB.id } });
      const result = await EmployeeModel.findOne({ _id: created._id }).populate({
        path: 'position',
        model: PositionModel,
        select: 'id name description'
      });
      return {
        success: true,
        message: 'Employee successfully saved',
        entity: this.mapToDomain(result.toObject())
      };
    } catch (err) {
      console.debug('EMPLOYEE_MONGO_REPOSITORY.SAVE.ERROR', { err, message: err.message, entity });
      throw new BusinessException({
        errorMessage: err.message,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
  }

  public async findOneByFilters(filters: Partial<Employee>): Promise<Employee | null> {
    console.debug('EMPLOYEE_MONGO_REPOSITORY.FIND_ONE_BY_FILTERS', { filters });
    try {
      const { id, position, ...otherFilters } = filters;
      const searchCriteria = isEmpty(id) ? otherFilters : { _id: Types.ObjectId.createFromHexString(id) };
      if (isEmpty(searchCriteria)) return null;

      const hasExist = await EmployeeModel.findOne(searchCriteria).populate({
        path: 'position',
        model: PositionModel,
        select: 'id name description'
      });
      return isEmpty(hasExist) ? null : this.mapToDomain(hasExist.toObject());
    } catch (err) {
      console.error('EMPLOYEE_MONGO_REPOSITORY.FIND_ONE_BY_FILTERS.ERROR', { err, message: err.message, filters });
      throw new BusinessException({
        errorMessage: err.message,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
  }

  public async update(entity: Partial<Employee>): Promise<ActionResult<Employee>> {
    console.debug('EMPLOYEE_MONGO_REPOSITORY.UPDATE', { entity });
    const { position } = entity;
    if (!isEmpty(position) && !isEmpty(position.id)) await this.getPositionByID(position.id);
    try {
      const { id, ...data } = entity;
      const positionId = position?.id;
      const set = isEmpty(positionId)
        ? { ...data }
        : {
            ...data,
            ...{ position: positionId }
          };
      const result = await EmployeeModel.updateOne({ _id: Types.ObjectId.createFromHexString(id) }, { $set: set });
      const updated = await this.findOneByFilters({ id });
      return {
        success: result.modifiedCount === 1,
        message: 'Employee successfully updated',
        entity: updated
      };
    } catch (err) {
      console.error('EMPLOYEE_MONGO_REPOSITORY.UPDATE.ERROR', { err, message: err.message, entity });
      throw new BusinessException({
        errorMessage: err.message,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
  }

  public async findAll(
    filters?: SearchFilters<{ stringency: SearchStringency } & Employee>
  ): Promise<Page<Array<Employee> | Array<never>>> {
    console.debug('EMPLOYEE_MONGO_REPOSITORY.FIND_ALL', { filters });
    let { stringency = SearchStringency.EXACTLY, page = 1, limit = 20, ...otherProperties } = filters ?? {};
    page = parseInt(page as unknown as string);
    limit = parseInt(limit as unknown as string);
    const query = otherProperties;

    Object.keys(query).forEach((key) => {
      if (!isEmpty(query[key]))
        query[key] =
          stringency === SearchStringency.EXACTLY
            ? (query[key] as string)
            : ({ $regex: query[key], $options: 'i' } as { $regex: string; $options: string });
    });
    const totalRecords = await EmployeeModel.countDocuments(query);
    MongoUtils.verifyPageRange(page, limit, totalRecords);
    try {
      const totalPages = Math.ceil(totalRecords / limit);
      const docs = await EmployeeModel.find(query)
        .populate({
          path: 'position',
          model: PositionModel,
          select: 'id name description'
        })
        .limit(limit)
        .skip((parseInt(page as unknown as string) - 1) * limit)
        .exec();
      const payload = isEmpty(docs) ? [] : docs.map((doc) => this.mapToDomain(doc.toObject()));

      return { totalRecords, totalPages, payload, limit, page };
    } catch (err) {
      console.error('EMPLOYEE_MONGO_REPOSITORY.FIND_ALL.ERROR', { err, message: err.message, filters });
      throw new BusinessException({
        errorMessage: err.message,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
  }

  public async delete(id: string): Promise<ActionResult<Employee>> {
    console.debug('EMPLOYEE_MONGO_REPOSITORY.DELETE', { id });
    let result = null;
    try {
      result = await EmployeeModel.findByIdAndDelete({ _id: Types.ObjectId.createFromHexString(id) }).populate({
        path: 'position',
        model: PositionModel,
        select: 'id name description'
      });
    } catch (err) {
      console.error('EMPLOYEE_MONGO_REPOSITORY.SAVE.DELETE', { err, message: err.message, id });
      throw new BusinessException({
        errorMessage: err.message,
        errorType: 'INTERNAL_SERVER_ERROR',
        code: HttpStatus.INTERNAL_SERVER_ERROR
      });
    }
    if (isEmpty(result))
      throw new BusinessException({
        errorMessage: `record not found with id: ${id}`,
        errorType: 'NOT_FOUND',
        code: HttpStatus.NOT_FOUND
      });

    return {
      success: true,
      message: 'Employee successfully delete',
      entity: this.mapToDomain(result.toObject())
    };
  }

  mapToDomain(data: Unknown): Employee {
    return new EmployeeBuilder().build(data);
  }

  private async getPositionByID(id: string): Promise<Position> {
    const positionInDB = await this.positionMongoRepository.findOneByFilters({ id });
    if (isEmpty(positionInDB))
      throw new BusinessException({
        errorMessage: `Position ID provided does not exist in db: ${id}.`,
        errorType: 'NOT_FOUND',
        code: HttpStatus.NOT_FOUND
      });
    return positionInDB;
  }
}
