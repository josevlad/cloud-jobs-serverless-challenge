import { BusinessException } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { HttpStatus } from '../../types';

export class MongoUtils {
  public static verifyPageRange(page: number, limit: number, totalRecords: number) {
    const totalPages = Math.ceil(totalRecords / limit);
    if (page < 1 || (totalPages > 0 && page > totalPages)) {
      throw new BusinessException({
        errorMessage: `page ${page} is out of range in the query`,
        errorType: 'PAGE_OUT_OF_RANGE',
        code: HttpStatus.BAD_REQUEST
      });
    }
  }
}
