import { Schema } from 'mongoose';

import { EmployeeDocument } from '../documents/employee.document';
import { Unknown } from '../../../types';

export const EmployeeSchema = new Schema<EmployeeDocument>(
  {
    name: { type: String, required: true },
    nationalIdentityCard: { type: Number, required: true },
    age: { type: Number, required: true },
    position: { type: Schema.Types.ObjectId, ref: 'Position', required: true }
  },
  {
    toObject: {
      transform: function (doc: Unknown, ret: Unknown) {
        ret.id = ret._id.toString();
        delete ret.__v;
        delete ret._id;
        if (ret.$setOnInsert) delete ret.$setOnInsert;
      }
    }
  }
);
