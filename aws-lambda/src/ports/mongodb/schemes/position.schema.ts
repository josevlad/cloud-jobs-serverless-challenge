import { Schema } from 'mongoose';

import { PositionDocument } from '../documents/position.document';
import { Unknown } from '../../../types';

export const PositionSchema = new Schema<PositionDocument>(
  {
    name: { type: String, required: true },
    description: { type: String, required: true }
  },
  {
    toObject: {
      transform: function (doc: Unknown, ret: Unknown) {
        ret.id = ret._id.toString();
        delete ret.__v;
        delete ret._id;
        if (ret.$setOnInsert) delete ret.$setOnInsert;
      }
    }
  }
);
