export type SearchFilters<T = undefined> = {
  limit?: number;
  page?: number;
} & (T extends undefined ? object : Partial<T>);
