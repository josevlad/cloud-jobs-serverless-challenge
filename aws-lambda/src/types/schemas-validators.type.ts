import { RuleValidatorType } from './rule-validator.type';

export type SchemasValidators = {
  id: string;
  schemasValidators: {
    [p: string]: {
      [key: string]: RuleValidatorType;
    };
  }[];
};
