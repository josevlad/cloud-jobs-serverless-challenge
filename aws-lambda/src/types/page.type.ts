export type Page<T> = {
  totalRecords: number;
  totalPages: number;
  limit: number;
  page: number;
  payload: T;
};
