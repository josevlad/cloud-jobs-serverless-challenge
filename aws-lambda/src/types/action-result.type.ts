export type ActionResult<T = undefined> = {
  success: boolean;
  message: string;
} & (T extends undefined ? { entity?: never } : { entity: Partial<T> });
