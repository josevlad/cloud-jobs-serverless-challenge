export type RuleValidatorType = {
  required?: boolean;
  minLength?: number;
  maxLength?: number;
  onlyLetters?: boolean;
  onlyNumbers?: boolean;
  min?: number;
  max?: number;
  pattern?: RegExp | string;
  allowedValues?: Array<unknown>;
};
