export type ApiGatewayEventDefaultAuthorizerContext = { [key: string]: any } | undefined | null;
