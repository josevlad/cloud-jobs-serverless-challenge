import { ApiGatewayProxyEventBase } from './api-gateway-proxy-event-base.interface';
import { ApiGatewayEventDefaultAuthorizerContext } from './api-gateway-event-default-authorizer-context.type';

type APIGatewayProxyEvent = ApiGatewayProxyEventBase<ApiGatewayEventDefaultAuthorizerContext>;

export type APIGatewayEvent = APIGatewayProxyEvent;
