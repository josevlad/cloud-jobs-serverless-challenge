import { APIGatewayEventRequestContextWithAuthorizer } from './api-gateway-event-request-context-with-authorizer.interface';

export interface ApiGatewayProxyEventBase<TAuthorizerContext> {
  body: string | null;
  headers: { [name: string]: string | undefined };
  multiValueHeaders: { [name: string]: string[] | undefined };
  httpMethod: string;
  isBase64Encoded: boolean;
  path: string;
  pathParameters: { [name: string]: string | undefined } | null;
  queryStringParameters: { [name: string]: string | undefined } | null;
  multiValueQueryStringParameters: { [name: string]: string[] | undefined } | null;
  stageVariables: { [name: string]: string | undefined } | null;
  requestContext: APIGatewayEventRequestContextWithAuthorizer<TAuthorizerContext>;
  resource: string;
}
