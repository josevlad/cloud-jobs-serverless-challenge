export interface ApiGatewayErrorType {
  errorType: unknown;
  errorMessage: unknown;
  statusCode?: unknown | null | undefined;
  data: unknown;
}
