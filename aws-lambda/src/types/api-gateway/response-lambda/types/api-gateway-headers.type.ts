export interface ApiGatewayHeadersType {
  [header: string]: boolean | number | string;
}
