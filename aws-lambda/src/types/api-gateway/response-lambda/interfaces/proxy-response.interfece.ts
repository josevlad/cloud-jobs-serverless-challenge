import { ApiGatewayProxyResultType } from '../types';

export interface ProxyResponseInterfece<T> {
  setResponse(response: T): void;
  getResponse(): ApiGatewayProxyResultType;
}
