import { ApiGatewayResponse } from './api-gateway.response';
import { ProxyResponseInterfece } from '../interfaces';
import { ApiGatewayHeadersType, ApiGatewayProxyResultType } from '../types';

export class ApiGatewayProxyResponse<T> implements ProxyResponseInterfece<ApiGatewayResponse<T>> {
  private readonly allowOrigin: string;

  constructor(allowOrigin?: string) {
    this.allowOrigin = allowOrigin ? allowOrigin : '*';
  }

  private response: ApiGatewayResponse<T>;

  setResponse(response: ApiGatewayResponse<T>): void {
    this.response = response;
  }

  getResponse(cors: boolean = true): ApiGatewayProxyResultType {
    const headers: ApiGatewayHeadersType = {};
    if (cors) {
      headers['Access-Control-Allow-Origin'] = this.allowOrigin;
      // TODO: hay que hacer una implementacion de configurar el Access-Control-Allow-Methods desde fuera, asi como el allowOrigin
      headers['Access-Control-Allow-Methods'] = 'OPTIONS,POST,GET,PUT,PATCH';
      headers['Access-Control-Allow-Credentials'] = 'true';
    }
    return {
      body: JSON.stringify(this.response.getBody()),
      statusCode: this.response.getStatusCode(),
      headers
    };
  }
}
