import { ApiGatewayResponse } from './api-gateway.response';
import { HttpStatus } from '../enums';

export class ApiGatewaySuccessfulResponse<T> extends ApiGatewayResponse<T> {
  constructor(body: T, statusCode: number = HttpStatus.OK) {
    super();
    this.setBody(body);
    this.setStatusCode(statusCode);
  }
}
