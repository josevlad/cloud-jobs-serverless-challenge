import { ApiGatewayResponse } from './api-gateway.response';
import { ApiGatewayErrorType } from '../types';
import { HttpStatus } from '../enums';

export class ApiGatewayErrorResponse<T> extends ApiGatewayResponse<T> {
  private errorType: unknown;

  private errorMessage: unknown;

  constructor(apiGatewayErrorType: ApiGatewayErrorType) {
    const { errorType, errorMessage, statusCode = HttpStatus.NOT_FOUND, data } = apiGatewayErrorType;
    super();
    this.setBody({ errorType, errorMessage: errorMessage, data } as unknown as T);
    this.setStatusCode(statusCode as number);
    this.errorType = errorType;
    this.errorMessage = errorMessage;
  }
}
