export abstract class ApiGatewayResponse<T> {
  protected body: T;

  protected statusCode: number;

  protected setBody(body: T): void {
    this.body = body;
  }

  public getBody(): T {
    return this.body;
  }

  protected setStatusCode(statusCode: number): void {
    this.statusCode = statusCode;
  }

  public getStatusCode(): number {
    return this.statusCode;
  }
}
