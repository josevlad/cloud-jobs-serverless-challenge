export enum SearchStringency {
  EXACTLY = 'exactly',
  CONTAINS = 'contains'
}
