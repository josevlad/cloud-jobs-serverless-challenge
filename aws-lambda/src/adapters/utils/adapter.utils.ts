import { format } from 'util';
import { BusinessException } from '@cencosud-ds/capacity-cross-architecture-ts-utils';
import { HttpStatus } from '../../types';

export class AdapterUtils {
  public static parseStringToObject<T>(json: string): T {
    console.debug('ADAPTER_UTILS.PARSE_STRING_TO_OBJECT', { json });
    try {
      return JSON.parse(json);
    } catch (err) {
      console.error('ADAPTER_UTILS.PARSE_STRING_TO_OBJECT.ERROR', JSON.stringify(err));
      throw new BusinessException({
        errorType: 'PARSE_STRING_TO_OBJECT_ERROR',
        errorMessage: `${format('Error when trying to map a text to an object: %s', json)}}.`,
        code: HttpStatus.BAD_REQUEST
      });
    }
  }
}
