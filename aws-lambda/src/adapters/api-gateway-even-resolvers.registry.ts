import { CreatePositionResolver } from './resolvers/create-position.resolver';
import { EmployeeMongoRepository, PositionMongoRepository } from '../ports';
import { ProxyResolverInterfece } from './interfaces';
import { Employee, Position } from '../domain';
import { EndpointsMap } from './endpoints.map';
import { ActionResult, APIGatewayEvent, Page } from '../types';
import { constants } from 'http2';
import { GetListPositionsResolver } from './resolvers/get-list-positions.resolver';
import { GetByIdPositionResolver } from './resolvers/get-by-id-position.resolver';
import { DeleteByIdPositionResolver } from './resolvers/delete-by-id-position.resolver';
import { UpdateByIdPositionResolver } from './resolvers/update-by-id-position.resolver';
import { CreateEmployeeResolver } from './resolvers/create-employee.resolver';
import { GetByIdEmployeeResolver } from './resolvers/get-by-id-employee.resolver';
import { DeleteByIdEmployeeResolver } from './resolvers/delete-by-id-employee.resolver';
import { GetListEmployeeResolver } from './resolvers/get-list-employee.resolver';
import { UpdateByIdEmployeeResolver } from './resolvers/update-by-id-employee.resolver';

type RegistryType = ProxyResolverInterfece<
  Employee | Position | ActionResult<Position | Employee> | Page<Array<Employee | Position>>
>;

export class ApiGatewayEvenResolversRegistry {
  private static readonly registry: Map<string, RegistryType> = new Map();

  public static registerResolvers() {
    const { HTTP2_METHOD_POST, HTTP2_METHOD_GET, HTTP2_METHOD_PUT, HTTP2_METHOD_DELETE } = constants;
    const positionMongoRepository = new PositionMongoRepository();
    const employeeMongoRepository = new EmployeeMongoRepository();

    this.registry.set(`${HTTP2_METHOD_POST}-${EndpointsMap.POSITION_PATH}`, new CreatePositionResolver(positionMongoRepository));
    this.registry.set(`${HTTP2_METHOD_GET}-${EndpointsMap.POSITION_PATH}`, new GetListPositionsResolver(positionMongoRepository));
    this.registry.set(
      `${HTTP2_METHOD_GET}-${EndpointsMap.POSITION_ID_PATH}`,
      new GetByIdPositionResolver(positionMongoRepository)
    );
    this.registry.set(
      `${HTTP2_METHOD_PUT}-${EndpointsMap.POSITION_ID_PATH}`,
      new UpdateByIdPositionResolver(positionMongoRepository)
    );
    this.registry.set(
      `${HTTP2_METHOD_DELETE}-${EndpointsMap.POSITION_ID_PATH}`,
      new DeleteByIdPositionResolver(positionMongoRepository)
    );
    //
    this.registry.set(`${HTTP2_METHOD_POST}-${EndpointsMap.EMPLOYEE_PATH}`, new CreateEmployeeResolver(employeeMongoRepository));
    this.registry.set(`${HTTP2_METHOD_GET}-${EndpointsMap.EMPLOYEE_PATH}`, new GetListEmployeeResolver(employeeMongoRepository));
    this.registry.set(
      `${HTTP2_METHOD_GET}-${EndpointsMap.EMPLOYEE_ID_PATH}`,
      new GetByIdEmployeeResolver(employeeMongoRepository)
    );
    this.registry.set(
      `${HTTP2_METHOD_PUT}-${EndpointsMap.EMPLOYEE_ID_PATH}`,
      new UpdateByIdEmployeeResolver(employeeMongoRepository)
    );
    this.registry.set(
      `${HTTP2_METHOD_DELETE}-${EndpointsMap.EMPLOYEE_ID_PATH}`,
      new DeleteByIdEmployeeResolver(employeeMongoRepository)
    );
  }

  public static getResolver({ httpMethod, resource }: APIGatewayEvent): RegistryType {
    this.registerResolvers();
    return this.registry.get(`${httpMethod}-${resource}`);
  }
}
