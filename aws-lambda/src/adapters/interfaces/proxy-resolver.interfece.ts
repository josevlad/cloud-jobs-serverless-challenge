import { APIGatewayEvent, ApiGatewayResponse } from '../../types';

export interface ProxyResolverInterfece<T> {
  resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<T>>;
}
