import { ProxyResolver } from './proxy.resolver';
import { ProxyResolverInterfece } from '../interfaces';
import { Employee } from '../../domain';
import { FindOneEmployeeUseCase } from '../../use-cases';
import { IRepository } from '../../ports';
import { APIGatewayEvent, ApiGatewayResponse, ApiGatewaySuccessfulResponse, HttpStatus } from '../../types';

export class GetByIdEmployeeResolver extends ProxyResolver implements ProxyResolverInterfece<Employee> {
  private useCase: FindOneEmployeeUseCase;

  constructor(repository: IRepository<Employee>) {
    super();
    this.useCase = new FindOneEmployeeUseCase(repository);
  }

  public async resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<Employee>> {
    await this.init();
    const { pathParameters } = event;
    const result = await this.useCase.execute(pathParameters);
    return new ApiGatewaySuccessfulResponse(result, HttpStatus.OK);
  }
}
