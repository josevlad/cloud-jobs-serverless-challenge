import { ProxyResolver } from './proxy.resolver';
import { ProxyResolverInterfece } from '../interfaces';
import { Position } from '../../domain';
import { FindOnePositionUseCase } from '../../use-cases';
import { IRepository } from '../../ports';
import { APIGatewayEvent, ApiGatewayResponse, ApiGatewaySuccessfulResponse, HttpStatus } from '../../types';

export class GetByIdPositionResolver extends ProxyResolver implements ProxyResolverInterfece<Position> {
  private useCase: FindOnePositionUseCase;

  constructor(repository: IRepository<Position>) {
    super();
    this.useCase = new FindOnePositionUseCase(repository);
  }

  public async resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<Position>> {
    await this.init();
    const { pathParameters } = event;
    const result = await this.useCase.execute(pathParameters);
    return new ApiGatewaySuccessfulResponse(result, HttpStatus.OK);
  }
}
