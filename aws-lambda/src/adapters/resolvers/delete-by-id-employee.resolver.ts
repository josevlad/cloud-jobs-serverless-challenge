import { ProxyResolver } from './proxy.resolver';
import { ProxyResolverInterfece } from '../interfaces';
import { Employee } from '../../domain';
import { DeleteEmployeeUseCase } from '../../use-cases';
import { IRepository } from '../../ports';
import { ActionResult, APIGatewayEvent, ApiGatewayResponse, ApiGatewaySuccessfulResponse, HttpStatus } from '../../types';

export class DeleteByIdEmployeeResolver extends ProxyResolver implements ProxyResolverInterfece<ActionResult<Employee>> {
  private useCase: DeleteEmployeeUseCase;

  constructor(repository: IRepository<Employee>) {
    super();
    this.useCase = new DeleteEmployeeUseCase(repository);
  }

  public async resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<ActionResult<Employee>>> {
    await this.init();
    const { pathParameters } = event;
    const result = await this.useCase.execute(pathParameters);
    return new ApiGatewaySuccessfulResponse(result, HttpStatus.OK);
  }
}
