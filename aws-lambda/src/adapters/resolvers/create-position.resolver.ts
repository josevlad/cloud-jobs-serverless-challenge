import {
  ActionResult,
  APIGatewayEvent,
  ApiGatewayResponse,
  ApiGatewaySuccessfulResponse,
  HttpStatus,
  Unknown
} from '../../types';
import { CreatePositionUseCase } from '../../use-cases';
import { ProxyResolverInterfece } from '../interfaces';
import { AdapterUtils } from '../utils/adapter.utils';
import { IRepository } from '../../ports';
import { Position } from '../../domain';
import { ProxyResolver } from './proxy.resolver';

export class CreatePositionResolver extends ProxyResolver implements ProxyResolverInterfece<ActionResult<Position>> {
  private useCase: CreatePositionUseCase;

  constructor(repository: IRepository<Position>) {
    super();
    this.useCase = new CreatePositionUseCase(repository);
  }

  public async resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<ActionResult<Position>>> {
    await this.init();
    const { body } = event;
    const data = AdapterUtils.parseStringToObject<Unknown>(body);
    const result = await this.useCase.execute(data);
    return new ApiGatewaySuccessfulResponse(result, HttpStatus.CREATED);
  }
}
