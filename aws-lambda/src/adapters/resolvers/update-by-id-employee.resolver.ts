import { ProxyResolver } from './proxy.resolver';
import { ProxyResolverInterfece } from '../interfaces';
import { Employee } from '../../domain';
import { UpdateEmployeeUseCase } from '../../use-cases';
import { IRepository } from '../../ports';
import {
  ActionResult,
  APIGatewayEvent,
  ApiGatewayResponse,
  ApiGatewaySuccessfulResponse,
  HttpStatus,
  Unknown
} from '../../types';
import { AdapterUtils } from '../utils/adapter.utils';

export class UpdateByIdEmployeeResolver extends ProxyResolver implements ProxyResolverInterfece<ActionResult<Employee>> {
  private useCase: UpdateEmployeeUseCase;

  constructor(repository: IRepository<Employee>) {
    super();
    this.useCase = new UpdateEmployeeUseCase(repository);
  }

  public async resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<ActionResult<Employee>>> {
    await this.init();
    const { pathParameters, body } = event;
    const data = AdapterUtils.parseStringToObject<Unknown>(body);
    const result = await this.useCase.execute({ ...pathParameters, ...data });
    return new ApiGatewaySuccessfulResponse(result, HttpStatus.OK);
  }
}
