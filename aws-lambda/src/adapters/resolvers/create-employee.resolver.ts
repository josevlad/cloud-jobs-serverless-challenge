import {
  ActionResult,
  APIGatewayEvent,
  ApiGatewayResponse,
  ApiGatewaySuccessfulResponse,
  HttpStatus,
  Unknown
} from '../../types';
import { CreateEmployeeUseCase } from '../../use-cases';
import { ProxyResolverInterfece } from '../interfaces';
import { AdapterUtils } from '../utils/adapter.utils';
import { ProxyResolver } from './proxy.resolver';
import { IRepository } from '../../ports';
import { Employee } from '../../domain';

export class CreateEmployeeResolver extends ProxyResolver implements ProxyResolverInterfece<ActionResult<Employee>> {
  private useCase: CreateEmployeeUseCase;

  constructor(repository: IRepository<Employee>) {
    super();
    this.useCase = new CreateEmployeeUseCase(repository);
  }

  public async resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<ActionResult<Employee>>> {
    await this.init();
    const { body } = event;
    const data = AdapterUtils.parseStringToObject<Unknown>(body);
    const result = await this.useCase.execute(data);
    return new ApiGatewaySuccessfulResponse(result, HttpStatus.CREATED);
  }
}
