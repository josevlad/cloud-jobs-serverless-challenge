import { ProxyResolver } from './proxy.resolver';
import { ProxyResolverInterfece } from '../interfaces';
import { Position } from '../../domain';
import { UpdatePositionUseCase } from '../../use-cases';
import { IRepository } from '../../ports';
import {
  ActionResult,
  APIGatewayEvent,
  ApiGatewayResponse,
  ApiGatewaySuccessfulResponse,
  HttpStatus,
  Unknown
} from '../../types';
import { AdapterUtils } from '../utils/adapter.utils';

export class UpdateByIdPositionResolver extends ProxyResolver implements ProxyResolverInterfece<ActionResult<Position>> {
  private useCase: UpdatePositionUseCase;

  constructor(repository: IRepository<Position>) {
    super();
    this.useCase = new UpdatePositionUseCase(repository);
  }

  public async resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<ActionResult<Position>>> {
    await this.init();
    const { pathParameters, body } = event;
    const data = AdapterUtils.parseStringToObject<Unknown>(body);
    const result = await this.useCase.execute({ ...pathParameters, ...data });
    return new ApiGatewaySuccessfulResponse(result, HttpStatus.OK);
  }
}
