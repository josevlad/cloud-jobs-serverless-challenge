import { ProxyResolver } from './proxy.resolver';
import { ProxyResolverInterfece } from '../interfaces';
import { Position } from '../../domain';
import { DeletePositionUseCase } from '../../use-cases';
import { IRepository } from '../../ports';
import { ActionResult, APIGatewayEvent, ApiGatewayResponse, ApiGatewaySuccessfulResponse, HttpStatus } from '../../types';

export class DeleteByIdPositionResolver extends ProxyResolver implements ProxyResolverInterfece<ActionResult<Position>> {
  private useCase: DeletePositionUseCase;

  constructor(repository: IRepository<Position>) {
    super();
    this.useCase = new DeletePositionUseCase(repository);
  }

  public async resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<ActionResult<Position>>> {
    await this.init();
    const { pathParameters } = event;
    const result = await this.useCase.execute(pathParameters);
    return new ApiGatewaySuccessfulResponse(result, HttpStatus.OK);
  }
}
