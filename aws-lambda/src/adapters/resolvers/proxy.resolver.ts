import { DataBaseService } from '../../services';
import { SchemaValidatorRegistry } from '../../domain';

export abstract class ProxyResolver {
  protected async init() {
    await DataBaseService.connect();
    await SchemaValidatorRegistry.registerSchemas();
  }
}
