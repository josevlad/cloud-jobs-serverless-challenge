import { APIGatewayEvent, ApiGatewayResponse, ApiGatewaySuccessfulResponse, HttpStatus, Page } from '../../types';
import { ProxyResolverInterfece } from '../interfaces';
import { ListPositionUseCase } from '../../use-cases';
import { ProxyResolver } from './proxy.resolver';
import { IRepository } from '../../ports';
import { Position } from '../../domain';

export class GetListPositionsResolver extends ProxyResolver implements ProxyResolverInterfece<Page<Array<Position>>> {
  private useCase: ListPositionUseCase;

  constructor(repository: IRepository<Position>) {
    super();
    this.useCase = new ListPositionUseCase(repository);
  }

  public async resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<Page<Array<Position>>>> {
    await this.init();
    const { queryStringParameters } = event;
    const result = await this.useCase.execute(queryStringParameters);
    return new ApiGatewaySuccessfulResponse(result, HttpStatus.OK);
  }
}
