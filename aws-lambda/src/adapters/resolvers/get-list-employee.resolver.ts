import { APIGatewayEvent, ApiGatewayResponse, ApiGatewaySuccessfulResponse, HttpStatus, Page } from '../../types';
import { ProxyResolverInterfece } from '../interfaces';
import { ListEmployeeUseCase } from '../../use-cases';
import { ProxyResolver } from './proxy.resolver';
import { IRepository } from '../../ports';
import { Employee } from '../../domain';

export class GetListEmployeeResolver extends ProxyResolver implements ProxyResolverInterfece<Page<Array<Employee>>> {
  private useCase: ListEmployeeUseCase;

  constructor(repository: IRepository<Employee>) {
    super();
    this.useCase = new ListEmployeeUseCase(repository);
  }

  public async resolve(event: APIGatewayEvent): Promise<ApiGatewayResponse<Page<Array<Employee>>>> {
    await this.init();
    const { queryStringParameters } = event;
    const result = await this.useCase.execute(queryStringParameters);
    return new ApiGatewaySuccessfulResponse(result, HttpStatus.OK);
  }
}
