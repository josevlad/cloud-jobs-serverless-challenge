export const EndpointsMap = {
  EMPLOYEE_PATH: '/cloud-jobs/employee',
  POSITION_PATH: '/cloud-jobs/position',
  EMPLOYEE_ID_PATH: '/cloud-jobs/employee/{id}',
  POSITION_ID_PATH: '/cloud-jobs/position/{id}'
} as const;
