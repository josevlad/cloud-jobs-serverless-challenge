@echo off

set LAMBDA_NAME=vjc-lambda-crud-employees

for /F "tokens=*" %%A in ('cd') do set "ROOT_DIR=%%A"

set AWS_LAMBDA_DIR=%ROOT_DIR%\aws-lambda
set AWS_LAMBDA_ZIPPED_DIR=%ROOT_DIR%\%LAMBDA_NAME%.zip
set OLD_DIST_DIR=%ROOT_DIR%\aws-lambda\dist

if exist "%AWS_LAMBDA_ZIPPED_DIR%" (
    del "%AWS_LAMBDA_ZIPPED_DIR%"
)

if exist "%OLD_DIST_DIR%" (
    del /Q "%OLD_DIST_DIR%"
)

cd /D "%AWS_LAMBDA_DIR%"
call npm run build:prod:win
cd /D "%AWS_LAMBDA_DIR%"\dist
powershell Compress-Archive -Path * -DestinationPath "%AWS_LAMBDA_ZIPPED_DIR%"
cd /D "%ROOT_DIR%"

if exist "%ROOT_DIR%"\output.txt (
    del "%ROOT_DIR%"\output.txt
)

aws lambda update-function-code --function-name %LAMBDA_NAME% --zip-file fileb://%LAMBDA_NAME%.zip --region us-east-1 --profile COMAFI_SANDBOX_ACCOUNT_MS > "%ROOT_DIR%"\output.txt

echo deploy successfully completed, see output.txt file

if exist "%AWS_LAMBDA_ZIPPED_DIR%" (
    del "%AWS_LAMBDA_ZIPPED_DIR%"
)

if exist "%OLD_DIST_DIR%" (
    del /Q "%OLD_DIST_DIR%"
)