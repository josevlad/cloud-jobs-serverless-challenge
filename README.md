### Proyecto Cloud Jobs API

Este proyecto es una API que permite la gestión de entidades `Employee` y `Position` con métodos HTTP para realizar operaciones de creación, lectura, actualización y eliminación (CRUD) sobre ellas. La arquitectura se ha diseñado siguiendo el patrón hexagonal, orientada al dominio y utiliza servicios proporcionados por AWS, como Lambda, API Gateway, DynamoDB y DocumentDB.

### Estructura del Proyecto

El proyecto está estructurado de acuerdo con la arquitectura hexagonal, lo que significa que las entidades son el centro y se dividen en capas con responsabilidades específicas:

- **Entidades**: `Employee` y `Position` representan los modelos principales del dominio.
- **Capa de Aplicación**: Contiene casos de uso específicos de la aplicación.
- **Capa de Infraestructura**: Contiene implementaciones de los puertos definidos en la capa de aplicación, interactuando con servicios externos como bases de datos.

### Endpoints API

Los endpoints disponibles son:

- **POST** `/cloud-jobs/employee`: Crea un nuevo empleado.
- **GET** `/cloud-jobs/employee`: Obtiene todos los empleados.
- **GET** `/cloud-jobs/employee/{id}`: Obtiene un empleado por su ID.
- **PUT** `/cloud-jobs/employee/{id}`: Actualiza un empleado existente.
- **DELETE** `/cloud-jobs/employee/{id}`: Elimina un empleado por su ID.

- **POST** `/cloud-jobs/position`: Crea una nueva posición.
- **GET** `/cloud-jobs/position`: Obtiene todas las posiciones.
- **GET** `/cloud-jobs/position/{id}`: Obtiene una posición por su ID.
- **PUT** `/cloud-jobs/position/{id}`: Actualiza una posición existente.
- **DELETE** `/cloud-jobs/position/{id}`: Elimina una posición por su ID.

### Ejemplos de JSON Body para POST y PUT

#### Position

```json
{
  "name": "Gerente de Ventas",
  "description": "Encargado de liderar el equipo de ventas y alcanzar los objetivos comerciales."
}
```


#### Employee

```json
{
  "name": "John Doe",
  "nationalIdentityCard": 123456789,
  "age": 30,
  "position": {...}
}
```
## Validación de Esquemas

```json
{
  "schemasValidators": [
    {
      "position": {
        "id": {
          "pattern": "/^[0-9a-fA-F]{24}$/"
        },
        "name": {
          "required": true,
          "pattern": "/^[a-zA-ZÀ-ÖØ-öø-ÿ0-9,.\\s:/]*$/",
          "minLength": 3
        },
        "description": {
          "pattern": "/^[a-zA-ZÀ-ÖØ-öø-ÿ0-9,.\\s:]*$/",
          "maxLength": 100
        }
      }
    },
    {
      "employee": {
        "id": {
          "pattern": "/^[0-9a-fA-F]{24}$/"
        },
        "name": {
          "required": true,
          "pattern": "/^[a-zA-ZÀ-ÖØ-öø-ÿ,.\\s:]*$/",
          "minLength": 3
        },
        "nationalIdentityCard": {
          "required": true,
          "onlyNumbers": true
        },
        "age": {
          "required": true,
          "onlyNumbers": true,
          "min": 18,
          "max": 65
        },
        "position": {
          "required": true
        }
      }
    }
  ]
}

```

### Postman Collection

Colección de Postman con ejemplos de solicitudes se encuentra [aquí](https://gitlab.com/josevlad/cloud-jobs-serverless-challenge/-/raw/develop/postman/Cloud_Jobs_API.postman_collection.json?inline=false).
